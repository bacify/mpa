<?php

namespace App\Models;

use CodeIgniter\Model;

class Mcom extends Model
{
    protected $table      = 'mesin';
    protected $primaryKey = 'id_m';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_m', 'seri','merk','keterangan'];
}