<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkabupaten extends Model
{
    protected $table      = 'regencies';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id', 'name','province_id'];
}