<?php

namespace App\Models;

use CodeIgniter\Model;

class Mservices extends Model
{
    protected $table      = 'mservices';
    protected $primaryKey = 'id_service';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_services', 'customer_id','com_id','meter_akhir','last_invoice_id','jumlah_tagihan','keterangan','tarif','tarif_ekstra','tarif_copy','free_copy','ppn','transportasi'];
}