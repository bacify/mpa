<?php

namespace App\Models;

use CodeIgniter\Model;

class Mdesa extends Model
{
    protected $table      = 'villages';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id', 'name','district_id'];
}