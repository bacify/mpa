<?php

namespace App\Models;

use CodeIgniter\Model;

class Mcustomer extends Model
{
    protected $table      = 'mcustomers';
    protected $primaryKey = 'id_customer';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_customer', 'nama','nomorkartu','jeniskartu','alamat','desa','kecamatan','kota','provinsi','telp','perusahaan'];
}