<?php

namespace App\Models;

use CodeIgniter\Model;

class Muser extends Model
{
    protected $table      = 'musers';
    protected $primaryKey = 'user_id';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $allowedFields = ['user_id', 'user_name','user_password'];
}