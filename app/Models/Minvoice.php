<?php

namespace App\Models;

use CodeIgniter\Model;

class Minvoice extends Model
{
    protected $table      = 'minvoices';
    protected $primaryKey = 'id_invoice';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['layanan_id', 'nama','nomorkartu','jeniskartu','alamat','desa','kecamatan','kota','provinsi','telp','seri','merk','tarif_sewa','tarif_ekstra','tarif_copy','free_copy','meter_awal','meter_akhir','rusak','ppn','bulan','keterangan','transportasi','perusahaan'];
}