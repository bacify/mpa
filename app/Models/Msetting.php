<?php

namespace App\Models;

use CodeIgniter\Model;

class Msetting extends Model
{
    protected $table      = 'msetting';
    protected $primaryKey = 'id';
    
    protected $useAutoIncrement = true;
    protected $allowedFields = ['id', 'nama','alamat','kota','provinsi','telp','kodepos','logo','ttd'];
}