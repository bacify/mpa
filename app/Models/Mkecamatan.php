<?php

namespace App\Models;

use CodeIgniter\Model;

class Mkecamatan extends Model
{
    protected $table      = 'districts';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id', 'name','regency_id'];
}