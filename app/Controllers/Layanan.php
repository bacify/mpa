<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mprovinsi;
use App\Models\Mcustomer;
use App\Models\Mcom;
use App\Models\Mservices;
use App\Models\Msetting;


class Layanan extends BaseController
{
	public function index()
	{
	
        $data['title']="Daftar Layanan Aktif";

        $provinsi= new Mprovinsi();
        
        $data['provinsi']= $provinsi->findAll();
        $mesin= new Mcom();        
        $data['mesin']= $mesin->findAll();       
        $data['bulan']= [ 1=>'Januari',
                          2=>'Februari',
                          3=>'Maret',
                          4=>'April',
                          5=>'Mei',
                          6=>'Juni',
                          7=>'Juli',
                          8=>'Agustus',
                          9=>'September',
                          10=>'Oktober',
                          11=>'Nopember',
                          12=>'Desember'];
                          $setting = new Msetting();
                          $data['setting'] = $setting->first();
       return view('v_layanan',$data);
	}

	public function ajaxdata()
    {
        $db = db_connect();
        $builder = $db->table('mservices')->select('concat_ws("/","SVC",YEAR(mservices.created_at),LPAD(MONTH(mservices.created_at),2,0),LPAD(id_service,4,0)) as service_code,id_service,c.nama,c.perusahaan,r.name,m.seri, jumlah_tagihan,last_invoice_id, i.created_at as t_invoice,mservices.transportasi')
                        ->where('mservices.deleted_at IS NULL')
                        ->join('mcustomers c','c.id_customer = mservices.customer_id')
                        ->join('regencies r','r.id = c.kota')
                        ->join('mesin m','m.id_m = mservices.com_id')
                        ->join('minvoices i','i.id_invoice = last_invoice_id' , 'left');                         

         
        return DataTable::of($builder) 
                ->setSearchableColumns(['concat_ws("/","SVC",YEAR(mservices.created_at),LPAD(MONTH(mservices.created_at),2,0),LPAD(id_service,4,0))','id_service','c.nama','c.perusahaan','r.name','m.seri'])                
                ->edit('last_invoice_id', function($row){
                    if($row->last_invoice_id==0)
                        return '-';
                    else{
                        if($row->transportasi == 0)
                            return '<button type="button" class="btn btn-link btn-sm" onclick="invoice_preview(\''.$row->last_invoice_id.'\')"><i class="fas fa-info"></i> INV/'.date('Y/m',strtotime($row->t_invoice)).'/'.sprintf('%04d',$row->last_invoice_id).' </button>';                        
                        else
                            return '<button type="button" class="btn btn-link btn-sm" onclick="invoice_preview(\''.$row->last_invoice_id.'\')"><i class="fas fa-info"></i> INV/'.date('Y/m',strtotime($row->t_invoice)).'/'.sprintf('%04d',$row->last_invoice_id).' </button> | 
                                <button type="button" class="btn btn-link btn-sm" onclick="invoice_transport_preview(\''.$row->last_invoice_id.'\')"><i class="fas fa-truck" ></button>';
                        }
                })
                ->add('action', function($row){
                    if($row->t_invoice == null)
                    return '<button type="button" class="btn btn-primary btn-sm" onclick="invoice('.$row->id_service.',0)" title="Buat Tagihan Baru"><i class="fas fa-file-invoice" ></i></button>
                    ';
                    else
                    return '<button type="button" class="btn btn-primary btn-sm" onclick="invoice('.$row->id_service.','.date('m',strtotime($row->t_invoice)).')" title="Buat Tagihan Baru"><i class="fas fa-file-invoice" ></i></button>
                    ';
                }, 'last')           
                ->add('action', function($row){
                    return '
                    <button type="button" class="btn btn-info btn-sm d-none" onclick="info_service('.$row->id_service.')" title="Detail Layanan"><i class="fas fa-info" ></i></button>
                    <button type="button" class="btn btn-primary btn-sm" onclick="edit_service('.$row->id_service.')" title="Ubah Layanan"><i class="fas fa-edit" ></i></button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="delete_service('.$row->id_service.')" title="Tutup Layanan"><i class="fas fa-times" ></i></button>';
                }, 'last')
                ->hide('id_service')
                ->hide('t_invoice')
                ->hide('transportasi')
               //->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function getdatabyid($id){

        if(!is_numeric($id)){
            $msg=[                 
                'status'=> 0,
                'pesan' => 'Error no id Found'];
        }
        else{
            $cs= new Mservices();     
            $data = $cs->where('id_service',$id)
                    ->join('mcustomers a','a.id_customer = customer_id')
                    ->first();
            if($data!=null){
                $msg=[                 
                    'status'=> 1,
                    'data'  => $data,
                    'pesan' => 'Ok!'];
            }else{
                $msg=[                 
                    'status'=> 0,                    
                    'pesan' => 'Data tidak ditemukan'];
            }
        }


            echo json_encode($msg);
    }

    public function new(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['customer' => 'required|numeric',
                                'com' => 'required|numeric',
                                'meter' => 'required|numeric',
                                'tarif' => 'required|numeric',
                                'tarif_eks' => 'required|numeric',
                                'tarif_copy' => 'required|numeric',
                                'freecopy' => 'required|numeric',  
                                'ppn' => 'required|numeric'  
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();         
        if($isDataValid){
            $cs = new Mservices();
            $result=$cs->save([
                "id_service"  => $this->request->getPost('id'),  
                "customer_id" => $this->request->getPost('customer'),
                "com_id" => $this->request->getPost('com'),
                "meter_akhir" => $this->request->getPost('meter'),
                "tarif" => $this->request->getPost('tarif'),
                "tarif_ekstra" => $this->request->getPost('tarif_eks'),
                "tarif_copy" => $this->request->getPost('tarif_copy'),
                "free_copy" => $this->request->getPost('freecopy'),
                "keterangan" => $this->request->getPost('keterangan'),                         
                "ppn" => $this->request->getPost('ppn'),                         
                "transportasi" => $this->request->getPost('transportasi'),                         
                "jumlah_tagihan" => 0,                         
            ]);
            if($result){
                if($this->request->getPost('id') == ''){
                    $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Layanan Baru Berhasil dibuat'];

                }else{
                     $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Layanan Berhasil di ubah'];
                }
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Layanan Gagal dibuat',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Layanan Gagal dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }

    public function newcs(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  [
                                'com' => 'required|numeric',
                                'meter' => 'required|numeric',
                                'tarif' => 'required|numeric',
                                'tarif_eks' => 'required|numeric',
                                'tarif_copy' => 'required|numeric',
                                'freecopy' => 'required|numeric',  
                                'ppn' => 'required|numeric',  
                                'transportasi' => 'required|numeric',  

                                'nama' => 'required',  
                                'identitas' => 'required',  
                                'no_identitas' => 'required|numeric',  
                                'alamat' => 'required',  
                                'provinsi' => 'required|numeric',  
                                'kota' => 'required|numeric',  
                                'kec' => 'required|numeric',  
                                'desa' => 'required|numeric',  
                                'telp' => 'required|numeric'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();         
        if($isDataValid){

            $user = new Mcustomer();
             $r=$user->insert([                     
                    "nama" => $this->request->getPost('nama'),
                    "perusahaan" => $this->request->getPost('perusahaan'),
                    "jeniskartu" => $this->request->getPost('identitas'),
                    "nomorkartu" => $this->request->getPost('no_identitas'),
                    "alamat" => $this->request->getPost('alamat'),
                    "desa" => $this->request->getPost('desa'),
                    "kecamatan" => $this->request->getPost('kec'),
                    "kota" => $this->request->getPost('kota'),
                    "provinsi" => $this->request->getPost('provinsi'),
                    "telp" => $this->request->getPost('telp')     
                ]);
            if($r){
                $iduser= $user->getInsertID();      
                $cs = new Mservices();
                $result=$cs->save([
                    "id_service"  => $this->request->getPost('id'),  
                    "customer_id" => $iduser,
                    "com_id" => $this->request->getPost('com'),
                    "meter_akhir" => $this->request->getPost('meter'),
                    "tarif" => $this->request->getPost('tarif'),
                    "tarif_ekstra" => $this->request->getPost('tarif_eks'),
                    "tarif_copy" => $this->request->getPost('tarif_copy'),
                    "free_copy" => $this->request->getPost('freecopy'),
                    "keterangan" => $this->request->getPost('keterangan'),                         
                    "ppn" => $this->request->getPost('ppn'),                         
                    "transportasi" => $this->request->getPost('transportasi'),                         
                    "jumlah_tagihan" => 0,                         
                ]);
                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$cs->getInsertID();
                        //echo 'valid';
                        $msg=[
                            'id'    =>$id,
                            'status'=> 1,
                            'pesan' => 'Layanan Baru Berhasil dibuat'];

                    }else{
                        $id=$cs->getInsertID();
                        //echo 'valid';
                        $msg=[
                            'id'    =>$id,
                            'status'=> 1,
                            'pesan' => 'Layanan Berhasil di ubah'];
                    }
                
                }else{
                    $msg=[                    
                        'status'=> 0,
                        'pesan ' => 'Layanan Gagal dibuat',
                        'error' => $cs->errors()];
                }
            }
            else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Pelanggan Gagal dibuat',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Layanan Gagal dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }

    public function delete(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Mservices();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
               
                    $msg=[                       
                        'status'=> 1,
                        'pesan' => 'Layanan Berhasil hentikan'];

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Layanan Gagal hentikan',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Layanan Gagal hentikan',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
}
