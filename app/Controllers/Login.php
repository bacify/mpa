<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Muser; 
 

class Login extends BaseController
{
	public function index()
    {
        if(session()->get('logged_in')){
            // maka redirct ke halaman login
            return redirect()->to('/panel/main'); 
        }
        helper(['form']);
        echo view('Login');
    } 
    public function auth()
    {
        
        $session = session();
       
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $m = new Muser();
        $d= $m->where('user_name',$username)->first();
         
        
        
        if($d){
            $pass = $d['user_password'];
            $verify_pass = password_verify($password, $pass);
            if($verify_pass){
                $ses_data = [
                    'user_id'       => $d['user_id'],
                    'user_name'     => $d['user_name'],                    
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/panel/main');
            }else{
                $session->setFlashdata('error', 'Wrong Password');
                return redirect()->to('/login');
            }
        }else{
            
            $session->setFlashdata('error', 'Username not Found');
            return redirect()->to('/login');
        }               

    }
 
    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

}