<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mprovinsi;
use App\Models\Mkabupaten;
use App\Models\Mkecamatan;
use App\Models\Mdesa;
use App\Models\Mcustomer;
use App\Models\Mservices;
use App\Models\Mcom;
use App\Models\Minvoice;
use CodeIgniter\API\ResponseTrait;

class Appdata extends BaseController
{
    use ResponseTrait;
	public function index()
	{
	
       return 'App Data';
	}

	public function getprovinsi($id=null)
    {
        $provinsi= new Mprovinsi();
        $req= $this->request->getGet();
        
        if($req)
        {
            if(isset($req['id']))
                $data = $provinsi->where('id',$req['id'])->findAll();
            else
                return $this->fail('Invalid Parameter', 400);
        }
        else{
            if($id==null)
                $data = $provinsi->findAll();
            else
                $data = $provinsi->where('id',$id)->findAll();
        }

        if($data)
            return $this->respond($data);
        else
            return $this->failNotFound('No Data Found with id '.$id);
    }

    public function getkabupaten($id=null)
    {
        $kabupaten= new Mkabupaten();
        $req= $this->request->getGet();
        
        if($req)
        {
            if(isset($req['id']))
                $data = $kabupaten->where('id',$req['id'])->findAll();
            else if(isset($req['prov']))
                $data = $kabupaten->where('province_id',$req['prov'])->findAll();
            else
                return $this->fail('Invalid Parameter', 400);
        }
        else{
            if($id==null)
                $data = $kabupaten->findAll();
            else
                $data = $kabupaten->where('id',$id)->findAll();
        }

        if($data)
            return $this->respond($data);
        else
            return $this->failNotFound('No Data Found with id '.$id);
    }


    public function getkecamatan($id=null)
    {
        $kecamatan= new Mkecamatan();
        $req= $this->request->getGet();
        
        if($req)
        {
            if(isset($req['id']))
                $data = $kecamatan->where('id',$req['id'])->findAll();
            else if(isset($req['kab']))
                $data = $kecamatan->where('regency_id',$req['kab'])->findAll();
            else
                return $this->fail('Invalid Parameter', 400);
        }
        else{
            if($id==null)
                $data = $kecamatan->findAll();
            else
                $data = $kecamatan->where('id',$id)->findAll();
        }

        if($data)
            return $this->respond($data);
        else
            return $this->failNotFound('No Data Found with id '.$id);
    }
    public function getdesa($id=null)
    {
        $desa= new Mdesa();
        $req= $this->request->getGet();
        
        if($req)
        {
            if(isset($req['id']))
                $data = $desa->where('id',$req['id'])->findAll();
            else if(isset($req['kec']))
                $data = $desa->where('district_id',$req['kec'])->findAll();
            else
                return $this->fail('Invalid Parameter', 400);
        }
        else{
            if($id==null)
                $data = $desa->findAll();
            else
                $data = $desa->where('id',$id)->findAll();
        }

        if($data)
            return $this->respond($data);
        else
            return $this->failNotFound('No Data Found with id '.$id);
    }

    public function getcustomer()
    {
        $cs= new Mcustomer();
        $req= $this->request->getGet();
        $con=array();
         
        $con = ['nama' => $req['term']];
        
        if($req)
        {            
                $data = $cs->like($con)->join('regencies a','a.id = kota')->findAll();
                
        }

        if($data){
            $ret= array();
            foreach($data as $d){
                $text = $d['nama'].' | '.$d['name'].' | '.$d['telp'];
                $ret[]= array('value' => $d['id_customer'] , 'label' =>$text);
            }
            
            return $this->respond(json_encode($ret));
        }
        else
            return $this->respond(json_encode(array()));
    }

    public function getservice()
    {
        $cs= new Mservices();
        $req= $this->request->getPost();
        $con=array();
         
        $con = ['id_service' => $req['id']];
        
        if($req)
        {            
                $data = $cs->like($con)
                        ->select('id_service,customer_id,com_id,c.nama,c.perusahaan,c.jeniskartu,c.nomorkartu,c.telp,c.alamat,r.name as kota,p.name as provinsi,d.name as kecamatan, v.name as desa, m.seri, m.merk,tarif, tarif_ekstra, tarif_copy, free_copy,meter_akhir,ppn,mservices.keterangan,transportasi ')
                        ->join('mcustomers c','c.id_customer = customer_id')
                        ->join('mesin m','m.id_m = com_id')
                        ->join('regencies r','r.id = c.kota')
                        ->join('provinces p','p.id = c.provinsi')
                        ->join('districts d','d.id = c.kecamatan')
                        ->join('villages v','v.id = c.desa')
                        ->findAll();
                
        }

        if($data){
            
            
            return $this->respond(json_encode($data));
        }
        else
            return $this->respond(json_encode(array()));
    }

    public function getinvoice()
    {
        $cs= new Minvoice();
        $req= $this->request->getPost();
        $con=array();
         
        $con = ['id_invoice' => $req['id']];
        
        if($req)
        {            
                $data = $cs->select('minvoices.*,mservices.created_at as date_service,mservices.customer_id')
                ->where($con)
                ->join('mservices','id_service = layanan_id')
                ->first();
                
        }

        if($data){
            
            
            return $this->respond(json_encode($data));
        }
        else
            return $this->respond(json_encode(array()));
    }
}
