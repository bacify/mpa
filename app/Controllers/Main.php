<?php

namespace App\Controllers;
use App\Models\Mcustomer; 
use App\Models\Minvoice; 
use App\Models\Mservices; 

class Main extends BaseController
{
	public function index()
	{
		$user = new Mcustomer();
		$data['user'] = $user->select('count(*) as jumlah')->first();
		$faktur = new Minvoice();
		$data['faktur'] = $faktur->select('count(*) as jumlah')->first();
		$services = new Mservices();
		$data['layanan'] = $services->select('count(*) as jumlah')->first();
		$layanan_faktur = new Mservices();
		$data['layanan_faktur'] = $layanan_faktur->select('count(*) as jumlah')
									->where( 'MONTH(updated_at) ' , date('n') - 1  )
									->where('YEAR(updated_at)',date('Y') )->first();
		$data['title']='Dashboard';
		return view('page',$data);
	}
}
