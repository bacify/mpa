<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Muser;


class Users extends BaseController
{
	public function index()
	{
	
        $data['title']="Daftar User App";
        
       return view('v_users',$data);
	}

	public function ajaxdata()
    {
        $db = db_connect();
        $builder = $db->table('musers')->select('user_id,user_name')->where('deleted_at IS NULL');

         
        return DataTable::of($builder)
                ->add('action', function($row){

                    if($row->user_id == 1)
                        return '';
                    else
                        return '<button type="button" class="btn btn-primary btn-sm" onclick="edit_person('.$row->user_id.',\''.$row->user_name.'\')" ><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="delete_person('.$row->user_id.',\''.$row->user_name.'\')" ><i class="fas fa-times"></i></button>';
                }, 'last')
                ->hide('user_id')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function getdatabyid($id){

        if(!is_numeric($id)){
            $msg=[                 
                'status'=> 0,
                'pesan' => 'Error no id Found'];
        }
        else{
            $cs= new Muser();     
            $data = $cs->where('user_id',$id)->first();
            if($data!=null){
                $msg=[                 
                    'status'=> 1,
                    'data'  => $data,
                    'pesan' => 'Ok!'];
            }else{
                $msg=[                 
                    'status'=> 0,                    
                    'pesan' => 'Data tidak ditemukan'];
            }
        }


            echo json_encode($msg);
    }

    public function new(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['username' => 'required',
                                'password' => 'required|min_length[8]'                           
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Muser();
            $r = $cs->where('user_name',$this->request->getPost('username'))->first();
            $id = $this->request->getPost('id');
            if($r &&  !$id){                

                    $msg =[
                        'status' => 0,
                        'pesan ' => 'GAGAL',
                        'error'  => 'Username Sudah Ada!!'
                    ];
                    echo json_encode($msg);
            }else{
                $cs = new Muser();          

                $password =  $this->request->getPost('password');
                $password_hash='';
                    if($password && (strlen($password) > 7)){                           
                        $password_hash=password_hash($this->request->getPost('password'), PASSWORD_DEFAULT)  ;               
                    }
                    
                    $result=$cs->save([
                        "user_id" => $this->request->getPost('id'),
                        "user_name" => $this->request->getPost('username') ,
                        "user_password" => $password_hash
                            
                    ]);

                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$cs->getInsertID();
                        //echo 'valid';
                        $msg=[
                            'id'    =>$id,
                            'status'=> 1,
                            'pesan' => 'User Baru Berhasil dibuat'];

                    }else{
                        $id=$cs->getInsertID();
                        //echo 'valid';
                        $msg=[
                            'id'    =>$id,
                            'status'=> 1,
                            'pesan' => 'User Berhasil di ubah'];
                    }
                }else{
                    $msg=[                    
                        'status'=> 0,
                        'pesan ' => 'User Gagal dibuat',
                        'error' => $cs->errors()];
                }
                echo json_encode($msg);
            }
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Pelanggan Gagal dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
    public function delete(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Musers();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
               
                    $msg=[                       
                        'status'=> 1,
                        'pesan' => 'User Berhasil dihapus'];

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'User Gagal dihapus',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'User Gagal dihapus',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
}
