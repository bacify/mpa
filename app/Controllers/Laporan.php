<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Minvoice; 


class Laporan extends BaseController
{
	public function index()
	{
        $bulan= ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
        
        $m = new Minvoice();
        $min_year =  $m->select('min(YEAR(created_at)) as tahun')->first();        

        $inputrange = range($min_year['tahun'],date('Y'));
        $data['tahun']= $inputrange;
        
        $m = new Minvoice();
        $reqt= $this->request->getVar('tahun', FILTER_SANITIZE_STRING);
        $reqb= $this->request->getVar('bulan', FILTER_SANITIZE_STRING);

        $tahun = date('Y');
        $b = date('n');
        if($reqt){
            $tahun = $reqt;
            $b = $reqb;
        }
        $data['bulan'] = $bulan;
        $data['title']="Laporan Bulan ".$bulan[$b-1]." ".$tahun;
        $data['invoice']=$m->select('week(created_at,1) as minggu,
                                sum(
                                    if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                        (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                        0)) as tambahan,
                                sum(tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                0)) as subtotal,
                                sum( (tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                0)) *ppn / 100) as pajak,
                                sum(transportasi) as tp')
                        ->where('YEAR(created_at)',$tahun)
                        ->where('month(created_at)',$b)
                        ->groupBy('minggu,month(created_at)')
                        ->findAll();

        
       return view('v_laporan1',$data);
	}

    public function tahun()
	{
        $bulan= ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
        $data['bulan'] = $bulan;
        $m = new Minvoice();
        $min_year =  $m->select('min(YEAR(created_at)) as tahun')->first();        

        $inputrange = range($min_year['tahun'],date('Y'));
        $data['tahun']= $inputrange;
        
        $m = new Minvoice();
        $req= $this->request->getVar('tahun', FILTER_SANITIZE_STRING);

        $tahun = date('Y');
        if($req){
            $tahun = $req;
        }
        $data['title']="Laporan Tahun ".$tahun;
        $data['invoice']=$m->select('month(created_at) bulan,
                                    sum(
                                        if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                            (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                            0)) as tambahan,
                                    sum(tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                    (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                    0)) as subtotal,
                                    sum( (tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                    (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                    0)) *ppn / 100) as pajak,
                                     sum(transportasi) as tp')
                            ->where('YEAR(created_at)',$tahun)
                            ->groupBy('month(created_at),year(created_at)')
                            ->findAll();
        
       return view('v_laporan2',$data);
	}
    public function tahun_by_invoice()
	{
        $bulan= ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
        $data['bulan'] = $bulan;
        $m = new Minvoice();
        $min_year =  $m->select('min(YEAR(created_at)) as tahun')->first();        

        $inputrange = range($min_year['tahun'],date('Y'));
        $data['tahun']= $inputrange;
        
        $m = new Minvoice();
        $req= $this->request->getVar('tahun', FILTER_SANITIZE_STRING);

        $tahun = date('Y');
        if($req){
            $tahun = $req;
        }
        $data['title']="Laporan Tahun ".$tahun;
        $data['invoice']=$m->select('bulan,
                                    sum(
                                        if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                            (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                            0)) as tambahan,
                                    sum(tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                    (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                    0)) as subtotal,
                                    sum( (tarif_sewa + tarif_copy + if( (meter_akhir - meter_awal - rusak)>free_copy, 
                                    (meter_akhir - meter_awal - free_copy - rusak ) * tarif_ekstra,
                                    0)) *ppn / 100) as pajak,
                                     sum(transportasi) as tp')
                            ->where('YEAR(created_at)',$tahun)
                            ->groupBy('bulan,year(created_at)')
                            ->findAll();
        
       return view('v_laporan3',$data);
	}
}