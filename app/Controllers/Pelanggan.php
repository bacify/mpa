<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mprovinsi;
use App\Models\Mcustomer;


class Pelanggan extends BaseController
{
	public function index()
	{
	
        $data['title']="Daftar Pelanggan";
        $provinsi= new Mprovinsi();
        
        $data['provinsi']= $provinsi->findAll();
       return view('v_pelanggan',$data);
	}

	public function ajaxdata()
    {
        $db = db_connect();
        $builder = $db->table('mcustomers')->select('id_customer,nama,perusahaan, alamat,r.name , telp,')->where('deleted_at IS NULL')->join('regencies r','r.id = mcustomers.kota');

         
        return DataTable::of($builder)
                ->add('action', function($row){
                    return '<button type="button" class="btn btn-primary btn-sm" onclick="edit_person('.$row->id_customer.')" ><i class="fas fa-edit"></i></button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="delete_person('.$row->id_customer.',\''.$row->nama.'\')" ><i class="fas fa-times"></i></button>';
                }, 'last')
                ->hide('id_customer')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function getdatabyid($id){

        if(!is_numeric($id)){
            $msg=[                 
                'status'=> 0,
                'pesan' => 'Error no id Found'];
        }
        else{
            $cs= new Mcustomer();     
            $data = $cs->where('id_customer',$id)->first();
            if($data!=null){
                $msg=[                 
                    'status'=> 1,
                    'data'  => $data,
                    'pesan' => 'Ok!'];
            }else{
                $msg=[                 
                    'status'=> 0,                    
                    'pesan' => 'Data tidak ditemukan'];
            }
        }


            echo json_encode($msg);
    }

    public function new(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['nama' => 'required',
                                'jeniskartu' => 'required',
                                'nomorkartu' => 'required|numeric',
                                'alamat' => 'required',
                                'provinsi' => 'required|numeric',
                                'kota' => 'required|numeric',
                                'kecamatan' => 'required|numeric',
                                'desa' => 'required|numeric',
                                'telp' => 'required|min_length[6]|numeric'                                
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Mcustomer();
            $result=$cs->save([
                "id_customer" => $this->request->getPost('id'),
                "nama" => $this->request->getPost('nama'),
                "jeniskartu" => $this->request->getPost('jeniskartu'),
                "nomorkartu" => $this->request->getPost('nomorkartu'),
                "alamat" => $this->request->getPost('alamat'),
                "desa" => $this->request->getPost('desa'),
                "kecamatan" => $this->request->getPost('kecamatan'),
                "kota" => $this->request->getPost('kota'),
                "provinsi" => $this->request->getPost('provinsi'),
                "telp" => $this->request->getPost('telp'),            
                "perusahaan" => $this->request->getPost('perusahaan')            
            ]);
            if($result){
                if($this->request->getPost('id') == ''){
                    $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Pelanggan Baru Berhasil dibuat'];

                }else{
                     $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Pelanggan Berhasil di ubah'];
                }
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Pelanggan Gagal dibuat',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Pelanggan Gagal dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
    public function delete(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Mcustomer();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
               
                    $msg=[                       
                        'status'=> 1,
                        'pesan' => 'Pelanggan Berhasil dihapus'];

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Pelanggan Gagal dihapus',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Pelanggan Gagal dihapus',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
}
