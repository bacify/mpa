<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mprovinsi;
use App\Models\Mcustomer;
use App\Models\Mcom;
use App\Models\Mservices;
use App\Models\Minvoice;
use App\Models\Msetting; 

class Tagihan extends BaseController
{
	public function index()
	{
	
        $data['title']="Data Tagihan";
        $setting = new Msetting();
        $data['setting'] = $setting->first();   
       return view('v_tagihan',$data);
	}

	public function ajaxdata()
    {
        $db = db_connect();
        $builder = $db->table('minvoices')
                    ->select('minvoices.created_at,
                                id_invoice,
                                concat_ws("/","INV",YEAR(minvoices.created_at),LPAD(MONTH(minvoices.created_at),2,0),LPAD(id_invoice,4,0)) as invoice_code,
                                
                                concat_ws("/","SVC",YEAR(mservices.created_at),LPAD(MONTH(mservices.created_at),2,0),LPAD(id_service,4,0)) as service_code,
                                bulan,nama,
                        (minvoices.meter_akhir - minvoices.meter_awal) as meter,
                        
                        if((minvoices.meter_akhir - minvoices.meter_awal - minvoices.rusak) >minvoices.free_copy,
                            (minvoices.meter_akhir - minvoices.meter_awal - rusak - minvoices.free_copy) *minvoices.tarif_ekstra,0)  as tambahan,
                            minvoices.tarif_sewa,
                            minvoices.tarif_copy,minvoices.ppn,
                            minvoices.transportasi ')
                        ->where('minvoices.deleted_at IS NULL')
                        ->join('mservices','id_service = layanan_id');
                        
                        
                         

         
        return DataTable::of($builder)
                ->setSearchableColumns(['concat_ws("/","INV",YEAR(minvoices.created_at),LPAD(MONTH(minvoices.created_at),2,0),LPAD(id_invoice,4,0))','concat_ws("/","SVC",YEAR(mservices.created_at),LPAD(MONTH(mservices.created_at),2,0),LPAD(id_service,4,0))', 'nama', 'bulan'])  
                ->edit('bulan', function($row){        
                    $b =[ 1=>'Januari',
                    2=>'Februari',
                    3=>'Maret',
                    4=>'April',
                    5=>'Mei',
                    6=>'Juni',
                    7=>'Juli',
                    8=>'Agustus',
                    9=>'September',
                    10=>'Oktober',
                    11=>'Nopember',
                    12=>'Desember'];            
                    return $b[$row->bulan];                
                }) 
                ->edit('tambahan', function($row){
                    
                    $tambahan=$row->tambahan;
                    $total = 0;
                
                    $subtotal = $row->tarif_sewa + $row->tarif_copy + $tambahan;
                    $ppn = $row->ppn/100;
                    $total = $subtotal *(1 + $ppn);
                    $total = 'Rp. '.number_format($total, 2, ',', '.');
                return $total;
                })                          
                ->add('action', function($row){
                    $r='';
                    $skrg = date('Y-m-d');                    
                    $inv = date('Y-m-d',strtotime(date('Y-m-d',strtotime($row->created_at)) . '+1 week'));                     
                    $r='<button type="button" class="btn btn-primary btn-sm" onclick="invoice('.$row->id_invoice.')" title="Lihat detail"><i class="fas fa-file-invoice" ></i></button>';
                    if( $row->transportasi > 0  )
                        $r .=' <button type="button" class="btn btn-info btn-sm" onclick="invoice_transport('.$row->id_invoice.')" title="Lihat detail"><i class="fas fa-truck" ></i></button>';
                           
                    if( !($skrg > $inv)  )
                        $r.=' <button type="button" class="btn btn-danger btn-sm" onclick="invoice_del('.$row->id_invoice.')" title="Hapus"><i class="fas fa-times" ></i></button>';

                    return $r;
                }, 'last')
                ->hide('tarif_sewa')                
                ->hide('tarif_copy')                
                ->hide('ppn')                
                ->hide('id_invoice')   
                ->hide('transportasi')   
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function getdatabyid($id){

        if(!is_numeric($id)){
            $msg=[                 
                'status'=> 0,
                'pesan' => 'Error no id Found'];
        }
        else{
            $cs= new Minvoice();     
            $data = $cs->select('minvoice.*,mservices.created_at as date_service')
                    ->where('id_invoice',$id)
                    ->join('mservices','id_service = layanan_id')
                    ->first();
            if($data!=null){
                $msg=[                 
                    'status'=> 1,
                    'data'  => $data,
                    'pesan' => 'Ok!'];
            }else{
                $msg=[                 
                    'status'=> 0,                    
                    'pesan' => 'Data tidak ditemukan'];
            }
        }


            echo json_encode($msg);
    }

    public function new(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric',
                                'nama' => 'required',
                                'alamat' => 'required',
                                'bulan' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Minvoice();
            $result=$cs->save([
                "layanan_id" => $this->request->getPost('id'),
                "nama" => $this->request->getPost('nama'),
                "perusahaan" => $this->request->getPost('perusahaan'),
                "nomorkartu" => $this->request->getPost('identitas'),
                "jeniskartu" => $this->request->getPost('tipe'),                         
                "alamat" => $this->request->getPost('alamat'),
                "desa" => $this->request->getPost('desa'),
                "kecamatan" => $this->request->getPost('kec'),                         
                "kota" => $this->request->getPost('kota'),                         
                "provinsi" => $this->request->getPost('prov'),                         
                "telp" => $this->request->getPost('telp'),                         
                "seri" => $this->request->getPost('seri'),                         
                "merk" => $this->request->getPost('merk'),                         
                "tarif_sewa" => $this->request->getPost('tarif'),                         
                "tarif_ekstra" => $this->request->getPost('tarif_ekstra'),                         
                "tarif_copy" => $this->request->getPost('tarif_copy'),                         
                "free_copy" => $this->request->getPost('free_copy'),                         
                "meter_awal" => $this->request->getPost('meter_awal'),                         
                "meter_akhir" => $this->request->getPost('meter_akhir'),                         
                "rusak" => $this->request->getPost('rusak'),                         
                "ppn" => $this->request->getPost('ppn'),                         
                "bulan" => $this->request->getPost('bulan'),                         
                "keterangan" => $this->request->getPost('keterangan'),                         
                "transportasi" => $this->request->getPost('transportasi')                         
            ]);
            if($result){
                
                $layanan= $this->request->getPost('id');
                $tg = new Minvoice();
                $a =$tg ->where("layanan_id",$layanan)->countAllResults();
                $id=$cs->getInsertID();


                $ly = new Mservices();
                $r=$ly->save([
                    'id_service' => $layanan,
                    'jumlah_tagihan'=> $a,
                    'last_invoice_id' =>$id,
                    'meter_akhir'   => $this->request->getPost('meter_akhir')
                ]);

                if($r){
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Tagihan Baru Berhasil dibuat'];
                }else{
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Tagihan Baru Berhasil dibuat, Layanan Gagal DiUpdate'];
                }                   

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Tagihan Gagal dibuat',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Layanan Gagal dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
    public function delete(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Minvoice();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
               
                    $msg=[                       
                        'status'=> 1,
                        'pesan' => 'Tagihan Berhasil dihapus'];

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Tagihan Gagal dihapus',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Tagihan Gagal dihapus',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }



    public function testing(){
        $tg = new Minvoice();
         $a =$tg ->where("layanan_id",1)->countAllResults();

         print_r($a);
    }
}
