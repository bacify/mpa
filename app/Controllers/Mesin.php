<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Mcom;


class Mesin extends BaseController
{
	public function index()
	{
	
        $data['title']="Data Mesin";
        
        return view('v_mesin',$data);
	}

	public function ajaxdata()
    {
        $db = db_connect();
        $builder = $db->table('mesin')->select('id_m,seri, merk, keterangan')->where('deleted_at IS NULL');

         
        return DataTable::of($builder)                
                ->add('action', function($row){
                    return '<button type="button" class="btn btn-primary btn-sm" onclick="edit_com('.$row->id_m.')" ><i class="fas fa-edit"></i></button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="delete_com('.$row->id_m.',\''.$row->seri.'\')" ><i class="fas fa-times"></i></button>';
                }, 'last')
                ->hide('id_m')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function getdatabyid($id){

        if(!is_numeric($id)){
            $msg=[                 
                'status'=> 0,
                'pesan' => 'Error no id Found'];
        }
        else{
            $cs= new Mcom();     
            $data = $cs->where('id_m',$id)->first();
            if($data!=null){
                $msg=[                 
                    'status'=> 1,
                    'data'  => $data,
                    'pesan' => 'Ok!'];
            }else{
                $msg=[                 
                    'status'=> 0,                    
                    'pesan' => 'Data tidak ditemukan'];
            }
        }


            echo json_encode($msg);
    }

    public function new(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['seri' => 'required',
                                'merk' => 'required',                                                                                           
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Mcom();
            $result=$cs->save([
                "id_m" => $this->request->getPost('id'),
                "seri" => $this->request->getPost('seri'),
                "merk" => $this->request->getPost('merk'),               
                "keterangan" => $this->request->getPost('keterangan')
            ]);
            if($result){
                if($this->request->getPost('id') == ''){
                    $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Mesin Baru Berhasil dibuat'];

                }else{
                     $id=$cs->getInsertID();
                    //echo 'valid';
                    $msg=[
                        'id'    =>$id,
                        'status'=> 1,
                        'pesan' => 'Data Mesin Berhasil di ubah'];
                }
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Mesin dibuat',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Mesin dibuat',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
    public function delete(){

        $validation =  \Config\Services::validation();
        $validation->setRules(  ['id' => 'required|numeric']
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $cs = new Mcom();
            $result=$cs->delete($this->request->getPost('id'));
            if($result){
               
                    $msg=[                       
                        'status'=> 1,
                        'pesan' => 'Mesin Berhasil dihapus'];

                
            }else{
                $msg=[                    
                    'status'=> 0,
                    'pesan ' => 'Mesin Gagal dihapus',
                    'error' => $cs->errors()];
            }
            echo json_encode($msg);
        }
        else{
            $errors = $validation->getErrors();

            $msg =[
                'status' => 0,
                'pesan ' => 'Pelanggan Gagal dihapus',
                'error'  =>$errors
            ];
            echo json_encode($msg);
        }

    }
}
