<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Msetting; 
use App\Models\Muser; 


class Setting extends BaseController
{
	public function index()
	{
        $data['title'] = "Pengaturan Aplikasi";
        $setting = new Msetting();
        $data['setting'] = $setting->first();
        return view('v_setting',$data);
    }



    public function save(){

        $password =  $this->request->getPost('password');
         
        if($password && (strlen($password) > 7)){
            $usermodel = new Muser();
            $usermodel->save([
                    'user_id'=>$this->request->getPost('iduser'),
                    'user_password'=> password_hash($this->request->getPost('password'), PASSWORD_DEFAULT)
                            ]);
            session()->setFlashdata('pass', 'password berhasil diubah');

             
        }
         
        $file = $this->request->getFile('gambar');
         
        if($file->isValid()){
            
            
            $file_type = $file->getClientMimeType();
            $valid_file_types = array("image/png", "image/jpeg", "image/jpg");
            $session = session();

            if (in_array($file_type, $valid_file_types)) {
            
                $setting = new Msetting();                
                $f = $setting->select('logo')->first();

                if(file_exists('uploads/img/'.$f['logo'])){
                    unlink('uploads/img/'.$f['logo']);

                }else{

                }

                $setting = new Msetting();                
                    $fileName = $file->getRandomName();
                    $setting->save([
                        'id' => $this->request->getPost('id'),
                        'nama' => $this->request->getPost('nama'),
                        'alamat' => $this->request->getPost('alamat'),
                        'kota' => $this->request->getPost('kota'),
                        'provinsi' => $this->request->getPost('prov'),
                        'kodepos' => $this->request->getPost('kodepos'),
                        'telp' => $this->request->getPost('telp'),
                        'ttd' => $this->request->getPost('ttd'),
                        'logo' => $fileName                        
                    ]);
                        $file->move('uploads/img/', $fileName);
                    session()->setFlashdata('success', 'Pengaturan Berhasil diubah');
            }else{
                session()->setFlashdata('error', json_encode(['filetype'=>'Ekstensi Tidak Di ijinkan']));
            }

            
        }else{
            
            $setting = new Msetting();    
            $setting->save([
                'id' => $this->request->getPost('id'),
                'nama' => $this->request->getPost('nama'),
                'alamat' => $this->request->getPost('alamat'),
                'kota' => $this->request->getPost('kota'),
                'provinsi' => $this->request->getPost('prov'),
                'kodepos' => $this->request->getPost('kodepos'),
                'ttd' => $this->request->getPost('ttd'),
                'telp' => $this->request->getPost('telp')
            ]);
            session()->setFlashdata('success', 'Pengaturan Berhasil diubah');
        }
	 return redirect()->to(base_url('panel/setting'));
    }



}