<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Login::index');

$routes->group('panel',['filter' => 'auth'], function($routes){
	$routes->get('main', 'Main::index');	
    $routes->get('client/', 'Pelanggan::index');
    $routes->add('client/new', 'Pelanggan::new');
    $routes->add('client/getdata/(:segment)', 'Pelanggan::getdatabyid/$1');   
	$routes->post('client/delete', 'Pelanggan::delete'); 	
	$routes->get('client/ajaxdata', 'Pelanggan::ajaxdata');	


	$routes->get('com/', 'Mesin::index');
    $routes->add('com/new', 'Mesin::new');
    $routes->add('com/getdata/(:segment)', 'Mesin::getdatabyid/$1');   
	$routes->post('com/delete', 'Mesin::delete'); 	
	$routes->get('com/ajaxdata', 'Mesin::ajaxdata');

	$routes->get('service/', 'Layanan::index');
    $routes->add('service/new', 'Layanan::new');
    $routes->add('service/newcs', 'Layanan::newcs');
    $routes->add('service/getdata/(:segment)', 'Layanan::getdatabyid/$1');   
	$routes->post('service/delete', 'Layanan::delete'); 	
	$routes->get('service/ajaxdata', 'Layanan::ajaxdata');


	$routes->get('invoices/', 'Tagihan::index');
    $routes->add('invoices/new', 'Tagihan::new');
    $routes->add('invoices/getdata/(:segment)', 'Tagihan::getdatabyid/$1');   
	$routes->post('invoices/delete', 'Tagihan::delete'); 	
	$routes->get('invoices/ajaxdata', 'Tagihan::ajaxdata');


	$routes->get('users/', 'Users::index');
    $routes->add('users/new', 'Users::new');
    $routes->add('users/getdata/(:segment)', 'Users::getdatabyid/$1');   
	$routes->post('users/delete', 'Users::delete'); 	
	$routes->get('users/ajaxdata', 'Users::ajaxdata');

	$routes->get('report1/', 'Laporan::index');    
	$routes->get('report2/', 'Laporan::tahun');    
	$routes->get('report3/', 'Laporan::tahun_by_invoice');    

	$routes->add('setting/', 'Setting::index');    
     
	
});
$routes->group('app', function($routes){
	$routes->get('provinsi/(:segment)', 'Appdata::getprovinsi/$1');
	$routes->get('provinsi', 'Appdata::getprovinsi');
	$routes->get('kabupaten/(:segment)', 'Appdata::getkabupaten/$1');
	$routes->get('kabupaten', 'Appdata::getkabupaten');
	$routes->get('kecamatan/(:segment)', 'Appdata::getkecamatan/$1');
	$routes->get('kecamatan', 'Appdata::getkecamatan');
	$routes->get('desa/(:segment)', 'Appdata::getdesa/$1');
	$routes->get('desa', 'Appdata::getdesa');
	$routes->get('data/customer', 'Appdata::getcustomer'); 	
	$routes->add('data/service', 'Appdata::getservice'); 	
	$routes->add('data/invoice', 'Appdata::getinvoice'); 	
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
