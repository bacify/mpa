<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        
<form class="form-inline">
  <label class="sr-only" for="tahun">tahun</label>
  <select  name="bulan" class="form-control form-control-sm" id="bulan" >
        <?php 
        $nn=1;
        foreach ($bulan as $m){
            echo '<option value="'.$nn++.'">'.$m.'</option>';
        }
        ?>
   </select>
   <select  name="tahun" class="form-control form-control-sm" id="tahun" >
        <?php 
        foreach ($tahun as $t){
            echo '<option value="'.$t.'">'.$t.'</option>';
        }
        ?>
   </select>
  <button type="submit" class="btn btn-dark btn-sm ml-1">Cari</button>
  <button type="button" class="btn btn-info btn-sm ml-1 " onclick="print_laporan()">Print</button> 
</form>
   
<hr>
        <table id="table-laporan" class="table table-striped table-bordered table-sm " cellspacing="0" width="100%">
            <thead>
                <tr>                    
                    <th class="text-center">Minggu</th>
                    <th class="text-center">Sewa</th>
                    <th class="text-center">Transportasi</th>                  
                    <th class="text-center">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $a =0;
                    $b=0;
                    $c=0;
                    $num=1;
                    foreach($invoice as $i){
                        $x =$i['subtotal'] + $i['pajak'] ;
                        $y = $i['tp'];
                        $a +=$x;
                        $b +=$y;
                        $c += ($x + $y);
                        echo '<tr> <td>'.$num++.'</td> 
                              <td class="text-right">'.number_format($x,2,',','.').'</td>
                              <td class="text-right">'.number_format($y,2,',','.').'</td>
                              <td class="text-right">'.number_format(($x + $y),2,',','.').'</td></tr>';
                    }
                    ?>
            </tbody>
            <tfoot><tr class="bg-info">
                    <td >Total </td>
                    <td class="text-right"> <?php echo number_format($a,2,',','.');?></td>
                    <td class="text-right"><?php echo number_format($b,2,',','.');?></td>
                    <td class="text-right"><?php echo number_format($c,2,',','.');?> </td>
                </tr></tfoot>
               
        </table>
   
 

<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  
$(document).ready(function() {

   
});


function print_laporan(){
        $('#table-laporan').print(
            {
                prepend  : '<h4><?=$title;?></h4>'
            }
        );
    }
</script>
 
<?= $this->endSection() ?>

