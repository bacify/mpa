 <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           
           <?php $uri= current_url(true); ?>
          <li class="nav-item">
            <a href="<?php echo base_url('main');?>" class="nav-link <?php if($uri->getSegment(2) == 'main' && $uri->getSegment(3)=='') echo 'active'; ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard                 
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('panel/client');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='client') echo 'active'; ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Data Pelanggan    
                         
              </p>
            </a>
             
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('panel/com');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='com') echo 'active'; ?>">
              <i class="nav-icon fas fa-server"></i>
              <p>
                Data Mesin                 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('panel/service');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='services') echo 'active'; ?>">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Kontrak Pelanggan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('panel/invoices');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='invoices') echo 'active'; ?>">
              <i class="nav-icon fas fa-file-invoice"></i>
              <p>
                Faktur
              </p>
            </a>
          </li>
           
          <li class="nav-item <?php if($uri->getSegment(3)=='report1' ||$uri->getSegment(3)=='report2' ) echo 'menu-is-opening menu-open'; ?>">
          <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file-archive"></i>
              <p>
                Laporan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a href="<?php echo base_url('panel/report1');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='report1') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulanan</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url('panel/report2');?>" class="nav-link <?php if($uri->getSegment(2) == 'panel' && $uri->getSegment(3)=='report2') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tahunan</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url('panel/report3');?>" class="nav-link <?php if($uri->getSegment(1) == 'panel' && $uri->getSegment(2)=='report3') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tahunan (by Tagihan)</p>
                </a>
              </li>
            </ul>
          </li>



          <li class="nav-item">
            <a href="<?php echo base_url('panel/setting');?>" class="nav-link <?php if($uri->getSegment(1) == 'panel' && $uri->getSegment(2)=='setting') echo 'active'; ?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
              </p>
            </a>
          </li>

          <?php 
            if(session()->get('user_id')== 1){
          ?>
          <li class="nav-item">
            <a href="<?php echo base_url('panel/users');?>" class="nav-link <?php if($uri->getSegment(1) == 'panel' && $uri->getSegment(2)=='users') echo 'active'; ?>">
              <i class="nav-icon fas fa-key"></i>
              <p>
                user
              </p>
            </a>
          </li>

          <?php } ?>
          <li class="nav-item">
            <a href="<?php echo base_url('login/logout');?>" class="nav-link <?php if($uri->getSegment(1) == 'login' && $uri->getSegment(2)=='logout') echo 'active'; ?>">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
               Keluar
              </p>
            </a>
          </li>

          <!-- <li class="nav-item">
            <a href="<?php echo base_url('main');?>" class="nav-link <?php if($uri->getSegment(1) == 'main' && $uri->getSegment(2)=='pelanggan') echo 'active'; ?>">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                Data Tagihan                 
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url('main');?>" class="nav-link <?php if($uri->getSegment(1) == 'main' && $uri->getSegment(2)=='pelanggan') echo 'active'; ?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan                 
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url('main/logout');?>" class="nav-link <?php if($uri->getSegment(1) == 'main' && $uri->getSegment(2)=='logout') echo 'active'; ?>">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                LogOut                 
              </p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->