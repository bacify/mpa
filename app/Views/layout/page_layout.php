<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PanduAPP | <?php if(isset($title)) echo $title; ?></title>
  <link rel="shortcut icon" type="image/jpg" href="<?php echo base_url('img/mpalogo.png');?>"/>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="<?php echo base_url('css/font.css');?>">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/fontawesome-free/css/all.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/ionicons-2.0.1/css/ionicons.min.css');?>">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('css/adminlte.min.css');?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/daterangepicker/daterangepicker.css');?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/summernote/summernote-bs4.min.css');?>">
  <!-- Datatables -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/datatables/datatables.min.css');?>">

  <!-- the fileinput plugin styling CSS file -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/bootstrap-fileinput/css/fileinput.min.css');?>">


  <!-- original mpa custom css -->
  <link rel="stylesheet" href="<?php echo base_url('css/mpa.css');?>">
  <!-- select 2 -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/select2/css/select2.min.css');?>">
  
  <link rel="stylesheet" href="<?php echo base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>">


  <link rel="stylesheet" href="<?php echo base_url('plugins/jquery-ui/jquery-ui.min.css');?>">

  

  <script>
      url = '<?php echo base_url();?>' ;
  </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
<div class="wrapper">

  <!-- Preloader -->
  <?php  $this->include("layout/splash");?>


  <!-- Top Header -->
  <?php echo $this->include('layout/navbar'); ?>
  <!-- /.top-header -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      
    <!-- Brand Logo -->
    <a href="<?= base_url();?>" class="brand-link">
      <img src="<?php echo base_url('img/mpalogo.png');?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">PanduApp</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      
      <!-- Sidebar Header -->
      <?php echo $this->include("layout/menuheader");?>
      <!-- /.sidebar-header -->

      <!-- Sidebar Menu -->
      <?php echo $this->include("layout/menu");?>
      <!-- /.sidebar-menu -->

    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?php if(isset($title)) echo $title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><?php if(isset($title)) echo $title; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- konten -->
      <?php $this->renderSection('content'); ?>


      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php echo $this->include("layout/footer");?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('plugins/jquery/jquery.min.js');?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('plugins/jquery-ui/jquery-ui.min.js');?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- additional library -->
<script src="<?php echo base_url('js/maspandu.js');?>"></script>
<?php $this->renderSection('jslibrary'); ?>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>


<!-- FILE INPUT -->
<!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="<?php echo base_url('plugins/bootstrap-fileinput/js/plugins/piexif.min.js');?>" type="text/javascript"></script>
 
 <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
     This must be loaded before fileinput.min.js -->
 <script src="<?php echo base_url('plugins/bootstrap-fileinput/js/plugins/sortable.min.js');?>" type="text/javascript"></script>
<!-- the main fileinput plugin script JS file -->
<script src="<?php echo base_url('plugins/bootstrap-fileinput/js/fileinput.min.js');?>"></script>

<!-- optionally if you need translation for your language then include the locale file as mentioned below (replace LANG.js with your language locale) -->
<script src="<?php echo base_url('plugins/bootstrap-fileinput/js/locales/id.js');?>"></script>


<!-- select 2 -->
<script src="<?php echo base_url('plugins/select2/js/select2.full.min.js');?>"></script>



<!-- JQUERY PRINT -->
<script src="<?php echo base_url('plugins/jquery.print/jQuery.print.min.js');?>"></script>

<!-- ChartJS -->
<!-- <script src="plugins/chart.js/Chart.min.js"></script> -->
<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> -->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script> -->
<!-- <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<!-- <script src="plugins/jquery-knob/jquery.knob.min.js"></script> -->
<!-- daterangepicker -->
<script src="<?php echo base_url('plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo base_url('plugins/daterangepicker/daterangepicker.js');?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>"></script>
<!-- Summernote -->
<script src="<?php echo base_url('plugins/summernote/summernote-bs4.min.js');?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('js/adminlte.js');?>"></script>



</body>
</html>
