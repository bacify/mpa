<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        
        <button class="btn btn-success btn-sm mb-2" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Pelanggan</button>
        <button class="btn btn-default btn-sm mb-2" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <hr>
        <table id="table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>                  
                    <th>Perusahaan</th>                  
                    <th>Alamat</th>                    
                    <th>kota</th>                   
                    <th>Telp</th>
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody></tbody>
               
        </table>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header">                
                <h3 class="modal-title">Pelanggan Baru</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="#" id="formpelanggan" class="form-horizontal" method="POST">
            <div class="modal-body ">
               
                    <input type="hidden" value="" name="id" id="iduser"/> 
                     
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <input id="inputnama" name="nama" placeholder="Mas Pandu " class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Perusahaan</label>
                            <div class="col-md-9">
                                <input id="inputperusahaan" name="perusahaan" placeholder="PT SINAR HARAPAN" class="form-control" type="text" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Identitas</label>
                            <div class="col-md-9">
                                <select id="inputidentitas" name="identitas" class="form-control" required>
                                    <option disabled selected>--pilih identitas--</option>
                                    <option value="ktp">KTP</option>
                                    <option value="sim">SIM</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Nomor</label>
                            <div class="col-md-9">
                                <input id="inputnomor" name="nomor" placeholder="31720000101111" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Address</label>
                            <div class="col-md-9">
                                <textarea id="inputalamat" name="alamat" placeholder="alamat" class="form-control" required></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Provinsi</label>
                            <div class="col-md-9">
                                <select id="inputprovinsi" name="provinsi" class="form-control" onChange="getState(this.value);" required>
                                <?php 
                                        if(isset($provinsi)){
                                            
                                             
                                            foreach($provinsi as $d){
                                                
                                            if($d['id']==34){
                                            ?>
                                            <option value="<?php echo $d['id']; ?>" selected><?php echo $d['name']; ?></option>
                                            
                                                <?php
                                            }
                                            else{
                                            ?>
                                            <option value="<?php echo $d['id']; ?>" ><?php echo $d['name']; ?></option>
                                            
                                        <?php 
                                            }
                                            }
                                        }else{
                                            ?>
                                            <option value="0">----</option>
                                            <?php
                                        }
                                        ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Kota</label>
                            <div class="col-md-9">
                                <select   class="form-control" id="kota" name="kota" onChange="getDistrict(this.value);" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Kecamatan</label>
                            <div class="col-md-9">
                                <select class="form-control" id="kec" name="kec" onChange="getVillage(this.value);" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Desa</label>
                            <div class="col-md-9">
                                <select  class="form-control" id="desa" name="desa" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Telepon</label>
                            <div class="col-md-9">
                                <input id="inputtelp"  name="telp" placeholder="081380007117" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                 
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" name="submit" class="btn btn-primary">simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<div class="modal" id="modal-delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" id="formhapus" class="form-horizontal" method="POST">
      <div class="modal-body">
        <p>Apakah Anda yakin akan menghapus pelanggan <b id="namapelanggan" > Ini </b> ?</p>
        <input type="hidden" id="idhapus" value="">
      </div>
      <div class="modal-footer">
        <button type="submit" id="btnDelete" class="btn btn-primary" name="submit" >Hapus Pelanggan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  

    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url();?>/plugins/datatables/lang/Indonesian.json" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/client/ajaxdata')?>",
            columnDefs: [
                { targets: 0, orderable: false}, //first column is not orderable.
                { targets: -1, className: 'text-center'}, //last column center.
            ]
        });
    });

    function add_person()
{
    save_method = 'add';
    resetform();
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Pelanggan Baru'); // Set Title to Bootstrap modal title
    

}
 function resetform(){

    $('#iduser').val(''); 
    $('#formpelanggan')[0].reset(); // reset form on modals
                let kota=$('#kota');
                let kec=$('#kec');
                let desa=$('#desa');
                kota.empty();
                kec.empty();
                desa.empty(); 
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
 }

 function edit_person(iduser)
{
        
    resetform();
    $('#iduser').val(iduser); 
    
    $.ajax({	
	url: url+"/panel/client/getdata/"+iduser	
    })
	.done(function(data){        
     
     let person = JSON.parse(data);
     $('#inputnama').val(person.data.nama);
     $('#inputidentitas').val(person.data.jeniskartu);
     $('#inputnomor').val(person.data.nomorkartu);
     $('#inputalamat').val(person.data.alamat);
     $('#inputperusahaan').val(person.data.perusahaan);

       getState(person.data.provinsi).then(()=>{
            getDistrict(person.data.kota).then(()=>{
                getVillage(person.data.kecamatan).then(()=>{
                   // console.log('okkk');
                        $('#inputprovinsi').find('option[value="'+person.data.provinsi+'"]').attr('selected','selected')
                        $('#kota').find('option[value="'+person.data.kota+'"]').attr('selected','selected')
                        $('#kec').find('option[value="'+person.data.kecamatan+'"]').attr('selected','selected')
                        $('#desa').find('option[value="'+person.data.desa+'"]').attr('selected','selected')
                        $('#inputtelp').val(person.data.telp);
                        $('#modal_form').modal('show'); // show bootstrap modal
                        $('.modal-title').text('Edit Data Pelanggan'); // Set Title to Bootstrap modal title
                    
                });
            });
       });
        
       
        
     
     
	})
    .fail(function(xhr,status,error) {
        alert( "error "+error);
        console.log(error);
    })
    .always(function(){
        
    });
    

}

function delete_person(iduser,namauser){
    $('#idhapus').val(iduser); 
    $('#namapelanggan').text(namauser);
    $('#modal-delete').modal('show'); // show bootstrap modal
}

$('#formpelanggan').submit(function(e){
    e.preventDefault();
    console.log('submit dimulai');
    let id=$('#iduser').val();
    let nama=$('#inputnama').val();
    let iden=$('#inputidentitas').val();
    let no_iden=$('#inputnomor').val();
    let alamat= $('#inputalamat').val();
    let provinsi = $('#inputprovinsi').val();
    let kota = $('#kota').val();
    let kec = $('#kec').val();
    let desa = $('#desa').val();
    let telp = $('#inputtelp').val();
    let company = $('#inputperusahaan').val();
   
        // add new pelanggan
        let data = { 
                id :id,
                nama : nama ,
                 jeniskartu : iden,
                 nomorkartu : no_iden,
                 alamat : alamat,
                 provinsi : provinsi,
                 kota : kota,
                 kecamatan : kec,
                 desa : desa,
                 telp : telp,
                 perusahaan : company
               };
         

        $.ajax({	
        url: url+"/panel/client/new",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetform();
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});


$('#formhapus').submit(function(e){
    e.preventDefault();
    console.log('hapus dimulai');
    let id=$('#idhapus').val();
    
        // add new pelanggan
        let data = { 
                id :id               
               };
         

        $.ajax({	
        url: url+"/panel/client/delete",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                console.log(data);
                alert( d.pesan );   
                $('#idhapus').val('');                
                $('#modal-delete').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});
    function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
    </script>
<?= $this->endSection() ?>

