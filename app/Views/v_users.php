<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        
        <button class="btn btn-success btn-sm mb-2" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah User</button>
        <button class="btn btn-default btn-sm mb-2" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <hr>
        <table id="table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Username</th>                                     
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody></tbody>
               
        </table>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header">                
                <h3 class="modal-title">User Baru</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="#" id="formuser" class="form-horizontal" method="POST">
            <div class="modal-body ">
               
                    <input type="hidden" value="" name="id" id="iduser"/> 
                     
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Username</label>
                            <div class="col-md-9">
                                <input id="username" name="username" placeholder="Pandu" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Password</label>
                            <div class="col-md-9">
                                <input id="password" name="password" placeholder="password" class="form-control" type="password" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                 
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" name="submit" class="btn btn-primary">simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<div class="modal" id="modal-delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" id="formhapus" class="form-horizontal" method="POST">
      <div class="modal-body">
        <p>Apakah Anda yakin akan menghapus pelanggan <b id="namapelanggan" > Ini </b> ?</p>
        <input type="hidden" id="idhapus" value="">
      </div>
      <div class="modal-footer">
        <button type="submit" id="btnDelete" class="btn btn-primary" name="submit" >Hapus Pelanggan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  

    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url();?>/plugins/datatables/lang/Indonesian.json" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/users/ajaxdata')?>",
            columnDefs: [
                { targets: 0, orderable: false}, //first column is not orderable.
                { targets: -1, className: 'text-center'}, //last column center.
            ]
        });
    });

    function add_person()
{
    
    resetform();
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('User Baru'); // Set Title to Bootstrap modal title
    

}
 function resetform(){

    $('#iduser').val(''); 
    $('#formuser')[0].reset(); // reset form on modals
                
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
 }

function edit_person(iduser,username)
{
        
    resetform();
    $('#iduser').val(iduser); 
    $('#username').val(username);    
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Edit Data Pelanggan'); // Set Title to Bootstrap modal title
      
    

}

function delete_person(iduser,namauser){
    $('#idhapus').val(iduser); 
    $('#namapelanggan').text(namauser);
    $('#modal-delete').modal('show'); // show bootstrap modal
}

$('#formuser').submit(function(e){
    e.preventDefault();
     
    let id=$('#iduser').val();
    let nama=$('#username').val();
    let password=$('#password').val();
     
        // add new pelanggan
        let data = { 
                id :id,
                username : nama ,
                password : password                  
               };
         

        $.ajax({	
        url: url+"/panel/users/new",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetform();
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});


$('#formhapus').submit(function(e){
    e.preventDefault();
    console.log('hapus dimulai');
    let id=$('#idhapus').val();
    
        // add new pelanggan
        let data = { 
                id :id               
               };
         

        $.ajax({	
        url: url+"/panel/users/delete",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                console.log(data);
                alert( d.pesan );   
                $('#idhapus').val('');                
                $('#modal-delete').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});
    function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
    </script>
<?= $this->endSection() ?>

