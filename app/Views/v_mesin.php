<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        
        <button class="btn btn-success btn-sm mb-2" onclick="add_com()"><i class="glyphicon glyphicon-plus"></i> Tambah Mesin</button>
        <button class="btn btn-default btn-sm mb-2" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <hr>
        <table id="table" class="table table-striped table-bordered table-sm " cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Seri</th>                  
                    <th>Merk</th>                 
                    
                    <th>keterangan</th>                     
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody></tbody>
               
        </table>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header">                
                <h3 class="modal-title">Mesin Baru</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="#" id="formmesin" class="form-horizontal" method="POST">
            <div class="modal-body ">
               
                    <input type="hidden" value="" name="id" id="idcom"/> 
                     
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Seri</label>
                            <div class="col-md-9">
                                <input id="inputseri" name="seri" placeholder="2004N" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Merk</label>
                            <div class="col-md-9">
                                <input id="inputmerk" name="merk" placeholder="Canon" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         
                       
                        
                         
                         
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Keterangan</label>
                            <div class="col-md-9">
                            <textarea id="inputketerangan" name="keterangan" placeholder="deskripsi" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                 
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSave" name="submit" class="btn btn-primary">simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<div class="modal" id="modal-delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" id="formhapus" class="form-horizontal" method="POST">
      <div class="modal-body">
        <p>Apakah Anda yakin akan menghapus mesin <b id="serimesin" > Ini </b> ?</p>
        <input type="hidden" id="idhapus" value="">
      </div>
      <div class="modal-footer">
        <button type="submit" id="btnDelete" class="btn btn-primary" name="submit" >Hapus Mesin</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  

    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url();?>/plugins/datatables/lang/Indonesian.json" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/com/ajaxdata')?>",
            
            columnDefs: [
                { targets: 0, orderable: false}, //first column is not orderable.
                { targets: -1, className: 'text-center'}, //last column center.
                { targets: 0, className: 'text-center'}, //last column center.
                
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: 'Data Mesin <?php echo date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: 'Data Mesin <?php echo date('d-m-Y'); ?> ',
                    messageTop :'Data Mesin <?php echo date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
    });

    function add_com()
{
    save_method = 'add';
    resetform();
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Mesin Baru'); // Set Title to Bootstrap modal title
    

}
 function resetform(){

    $('#idcom').val(''); 
    $('#formmesin')[0].reset(); // reset form on modals
     
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
 }

function edit_com(iduser)
{
        
    resetform();
    $('#idcom').val(iduser); 
    
    $.ajax({	
	url: url+"/panel/com/getdata/"+iduser	
    })
	.done(function(data){        
	 console.log(data);       

     
     let person = JSON.parse(data);
     $('#inputseri').val(person.data.seri);
     $('#inputmerk').val(person.data.merk);  
     $('#inputketerangan').val(person.data.keterangan);

     
          
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Edit Data Mesin'); // Set Title to Bootstrap modal title
	})
    .fail(function(xhr,status,error) {
        alert( "error "+error);
        console.log(error);
    })
    .always(function(){
        
    });
    

}

function delete_com(iduser,namauser){
    $('#idhapus').val(iduser); 
    $('#serimesin').text(namauser);
    $('#modal-delete').modal('show'); // show bootstrap modal
}

$('#formmesin').submit(function(e){
    e.preventDefault();
    console.log('submit dimulai');
    let id=$('#idcom').val();
    let seri=$('#inputseri').val();
    let merk=$('#inputmerk').val();
   
    let keterangan= $('#inputketerangan').val();
    
   
        // add new pelanggan
        let data = { 
                id :id,
                seri : seri ,
                merk : merk,
               
                keterangan : keterangan
               };
         

        $.ajax({	
        url: url+"/panel/com/new",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetform();
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});


$('#formhapus').submit(function(e){
    e.preventDefault();
    console.log('hapus dimulai');
    let id=$('#idhapus').val();
    
        // add new pelanggan
        let data = { 
                id :id               
               };
         

        $.ajax({	
        url: url+"/panel/com/delete",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                console.log(data);
                alert( d.pesan );   
                $('#idhapus').val('');                
                $('#modal-delete').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});
    function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
    </script>
<?= $this->endSection() ?>

