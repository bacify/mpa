<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        

<hr>
<div class="row">
		<div class="col-lg-6 col-sm-12">			 
        <?php if (!empty(session()->getFlashdata('error'))) : ?>
                    <div class="alert alert-danger" role="alert">
                         
                        <?php echo session()->getFlashdata('error'); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty(session()->getFlashdata('success'))) : ?>
                    <div class="alert alert-success" role="alert">
                         
                        <?php echo session()->getFlashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty(session()->getFlashdata('pass'))) : ?>
                    <div class="alert alert-info" role="alert">                         
                        <?php echo session()->getFlashdata('pass'); ?>
                    </div>
                <?php endif; ?>
			<form class="form " action="<?=base_url('setting/save')?>" method="POST" enctype="multipart/form-data">
			<table class="table table-sm">
				<thead><tr>
							<th> No</th>
							<th> Setting </th>
							<th> Value</th>							
						</tr>
				</thead>
				<tbody>				 
                    <tr><td>1</td>
						<td>Nama Instansi</td>
						<td>
                        <input type="hidden" name="id" value="<?=$setting['id'];?>">							   						                              
                            <input type="text" class="form-control form-control-sm" id="nama" name="nama" placeholder="CV SINAR FAJAR" value="<?=$setting['nama'];?>">							   						 
						</td>
					</tr>
                    <tr><td>2</td>
						<td>Alamat</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="alamat" name="alamat" placeholder="Jalan Gajahmada 06 Mojosari" value="<?=$setting['alamat'];?>">							   						 
						</td>
					</tr>	
                    <tr><td>3</td>
						<td>Kota</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="kota" name="kota" placeholder="Mojokerto" value="<?=$setting['kota'];?>">							   						 
						</td>
					</tr>	
                    <tr><td>4</td>
						<td>Provinsi</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="prov" name="prov" placeholder="Jawa Timur" value="<?=$setting['provinsi'];?>">							   						 
						</td>
					</tr>	
                    <tr><td>5</td>
						<td>Kodepos</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="kodepos" name="kodepos" placeholder="61382" value="<?=$setting['kodepos'];?>">							   						 
						</td>
					</tr>
                    <tr><td>6</td>
						<td>No Telp</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="telp" name="telp" placeholder="0813005000" value="<?=$setting['telp'];?>">							   						 
						</td>
					</tr>	
                    <tr><td>7</td>
						<td>Logo</td>
						<td>                             
                         
                        <input type="file" id="gambar" name="gambar">
                             
						</td>
					</tr>
					<tr><td>8</td>
						<td>Tanda tangan</td>
						<td>                             
                            <input type="text" class="form-control form-control-sm" id="ttd" name="ttd" placeholder="Pandu" value="<?=$setting['ttd'];?>">							   						 
						</td>
					</tr>		
                    <tr><td>9</td>
						<td>Username</td>
						<td>       
                        <input type="hidden" name="iduser" value="<?=session()->get('user_id');?>">                      
                            <input type="text" class="form-control form-control-sm" id="username" name="username" value="<?=session()->get('user_name');?>" readonly>							   						 
						</td>
					</tr>		
					<tr><td>10</td>
						<td>Password</td>
						<td>                             
                            <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Isi Untuk Mengubah">
						</td>
					</tr>				
				</tbody>
			</table>			
			<button class="btn btn-success float-right" type="submit" name="submit" value="save">Simpan</button>
			</form>
		</div>
	</div>   
 

<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  
$(document).ready(function() {
    $("#gambar").fileinput({showCaption: false, dropZoneEnabled: false});
   
});

</script>
 
<?= $this->endSection() ?>



