<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <button class="btn btn-success btn-sm mb-2" onclick="add_service()"><i class="glyphicon glyphicon-plus"></i> Layanan Baru</button>
        <button class="btn btn-default btn-sm mb-2" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <hr>
        <table id="table" class="table table-striped table-bordered table-sm " cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No Layanan</th>
                    <th>Pelanggan</th>                  
                    <th>Perusahaan</th>                  
                    <th>Kota</th>                    
                    <th>Seri</th>                                                          
                    <th>Tagihan</th>
                    <th>Tagihan Akhir</th>
                    <th>Tagihan</th>                                        
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody></tbody>
               
        </table>
        
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header">                
                <h3 class="modal-title">Layanan Baru </h3>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
            <form action="#" id="formlayanan" class="form-horizontal" method="POST">
            
                <div class="modal-body ui-front">      
                <div class="clearfix mb-1">
                <button type="button" class="btn btn-info btn-sm float-right" onclick="gantipelanggan()"> Pelanggan Baru</button>     
                </div>
                                        
                    <input type="hidden" value="" name="idpelanggan" id="idpelanggan"/>
                    <input type="hidden" value="" name="id_service" id="id_service"/>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Nama Pelanggan</label>
                            <div class="col-md-9">
                                <input id="inputnama" name="nama" placeholder="Pandu W" class="form-control" type="text" required>
                                <small id="passwordHelpBlock" class="form-text text-muted">ketik nama pelanggan / nomor telp </small>
                                <span class="help-block">ketik nama pelanggan atau nomor telp</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Jenis Mesin</label>
                            <div class="col-md-9">
                            <select id="inputcom" class="form-control" name="com" required>
                            <option selected="selected" disabled="true"></option>
                                <?php 
                                    foreach($mesin as $d){
                                        echo '<option value="'.$d['id_m'].'" >'.$d['seri'].' | '.$d['merk'].'</option>';
                                    }                                 
                                ?>                                
                            </select>                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Meter Awal</label>
                            <div class="col-md-9">
                                <input id="inputmeter" name="meter" placeholder="12345678" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Sewa</label>
                            <div class="col-md-9">
                                <input id="inputtarif" name="tarif" placeholder="100000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya ekstra</label>
                            <div class="col-md-9">
                                <input id="inputtarifekstra" name="tarifekstra" placeholder="1000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Free Copy</label>
                            <div class="col-md-9">
                                <input id="inputtarifcopy" name="tarifcopya" placeholder="100000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Free Copy</label>
                            <div class="col-md-9">
                                <input id="inputcopy" name="copy" placeholder="100" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Ppn</label>
                            <div class="col-md-9">
                                <input id="inputppn" name="ppn" placeholder="10" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         
                       
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">keterangan</label>
                            <div class="col-md-9">
                                <textarea id="inputketerangan" name="keterangan" placeholder="keterangan" class="form-control" ></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Transport</label>
                            <div class="col-md-9">
                                <input type="number" id="inputtransport" name="transport" placeholder="keterangan" class="form-control" ></input>
                                <span class="help-block"></span>
                            </div>
                        </div>
                 
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSave" name="submit" class="btn btn-primary">simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
                </div>
            </form>

            <form action="#" id="formlayananpelangganbaru" style="display:none" class="form-horizontal" method="POST">
            
                <div class="modal-body ui-front">      
                <div class="clearfix mb-1">
                    <button type="button" class="btn btn-info btn-sm float-right" onclick="gantipelanggan()"> pelanggan lama</button>     
                </div>
                                        
                    
                    <input type="hidden" value="" name="id_service" id="n-id_service"/>
                    <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <input id="n-inputnama" name="nama" placeholder="Mas Pandu " class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Perusahaan</label>
                            <div class="col-md-9">
                                <input id="n-inputperusahaan" name="perusahaan" placeholder="PT SINAR MAS" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Identitas</label>
                            <div class="col-md-9">
                                <select id="n-inputidentitas" name="identitas" class="form-control" required>
                                    <option disabled selected>--pilih identitas--</option>
                                    <option value="ktp">KTP</option>
                                    <option value="sim">SIM</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Nomor</label>
                            <div class="col-md-9">
                                <input id="n-inputnomor" name="nomor" placeholder="31720000101111" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Address</label>
                            <div class="col-md-9">
                                <textarea id="n-inputalamat" name="alamat" placeholder="alamat" class="form-control" required></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Provinsi</label>
                            <div class="col-md-9">
                                <select id="n-inputprovinsi" name="provinsi" class="form-control" onChange="ngetState(this.value);" required>
                                <?php 
                                        if(isset($provinsi)){
                                            
                                             
                                            foreach($provinsi as $d){
                                                
                                            if($d['id']==34){
                                            ?>
                                            <option value="<?php echo $d['id']; ?>" selected><?php echo $d['name']; ?></option>
                                            
                                                <?php
                                            }
                                            else{
                                            ?>
                                            <option value="<?php echo $d['id']; ?>" ><?php echo $d['name']; ?></option>
                                            
                                        <?php 
                                            }
                                            }
                                        }else{
                                            ?>
                                            <option value="0">----</option>
                                            <?php
                                        }
                                        ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Kota</label>
                            <div class="col-md-9">
                                <select   class="form-control" id="n-kota" name="kota" onChange="ngetDistrict(this.value);" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Kecamatan</label>
                            <div class="col-md-9">
                                <select class="form-control" id="n-kec" name="kec" onChange="ngetVillage(this.value);" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Desa</label>
                            <div class="col-md-9">
                                <select  class="form-control" id="n-desa" name="desa" required>
                                    <option value="0">---</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Telepon</label>
                            <div class="col-md-9">
                                <input id="inputtelp"  name="telp" placeholder="081380007117" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Jenis Mesin</label>
                            <div class="col-md-9">
                            <select id="n-inputcom" class="form-control" name="com" required>
                            <option selected="selected" disabled="true"></option>
                                <?php 
                                    foreach($mesin as $d){
                                        echo '<option value="'.$d['id_m'].'" >'.$d['seri'].' | '.$d['merk'].'</option>';
                                    }                                 
                                ?>                                
                            </select>                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Meter Awal</label>
                            <div class="col-md-9">
                                <input id="n-inputmeter" name="meter" placeholder="12345678" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Sewa</label>
                            <div class="col-md-9">
                                <input id="n-inputtarif" name="tarif" placeholder="100000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya ekstra</label>
                            <div class="col-md-9">
                                <input id="n-inputtarifekstra" name="tarifekstra" placeholder="1000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Free Copy</label>
                            <div class="col-md-9">
                                <input id="n-inputtarifcopy" name="tarifcopya" placeholder="100000" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Free Copy</label>
                            <div class="col-md-9">
                                <input id="n-inputcopy" name="copy" placeholder="100" class="form-control" type="number" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Ppn</label>
                            <div class="col-md-9">
                                <input id="n-inputppn" name="ppn" placeholder="10" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         
                       
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">keterangan</label>
                            <div class="col-md-9">
                                <textarea id="n-inputketerangan" name="keterangan" placeholder="keterangan" class="form-control" ></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="  col-md-3 col-form-label">Biaya Transport</label>
                            <div class="col-md-9">
                                <input type="number" id="n-inputtransport" name="transport" placeholder="keterangan" class="form-control" value="0" ></input>
                                <span class="help-block"></span>
                            </div>
                        </div>
                 
                </div>
                <div class="modal-footer">
                    <button type="submit" id="n-btnSave" name="submit" class="btn btn-primary">simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<div class="modal" id="modal-delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" id="formhapus" class="form-horizontal" method="POST">
      <div class="modal-body">
        <p>Apakah Anda yakin akan menghapus Layanan <b id="idlayanan" > Ini </b> ?</p>
        <input type="hidden" id="idhapus" value="">
      </div>
      <div class="modal-footer">
        <button type="submit" id="btnDelete" class="btn btn-primary" name="submit" >Hapus Mesin</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_invoice" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header">                
                <h3 class="modal-title">Tagihan Baru </h3>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
            <form action="#" id="forminvoice" class="form-horizontal" method="POST">
            <input type="hidden" value="" name="id" id="idservice"/> 
            <input type="hidden" value="" name="bulanakhir" id="bulanakhir"/> 
            <div class="modal-body bg-light">   
                <div class="text-center"> <strong class="text-primary">Data Layanan</strong> </div> 
                <div class="form-row">                                       
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="seri">Seri Mesin</small>
                            <input type="text" class="form-control form-control-sm" id="seri" name="seri" placeholder="A2021" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="merk">Merk</small>
                            <input type="text" class="form-control form-control-sm" id="merk" name="merk" placeholder="CANON" readonly>
                        </div>                        
                </div> 
                
            </div>

            <div class="modal-body bg-white ui-front">        
                <div class="text-center"> <strong class="text-secondary">Data Pelanggan</strong> </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="nama">Nama</small>
                            <input type="text" class="form-control form-control-sm" id="nama" name="nama" placeholder="Nama" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">telp</small>
                            <input type="text" class="form-control form-control-sm" id="telp" name="telp" placeholder="telp" readonly>
                        </div>                        
                    </div>
                    <div class="form-row">                        
                        <div class="form-group col-md-12">
                            <small class="font-weight-bold" for="telp">Keterangan</small>
                            <textarea class="form-control form-control-sm" id="keterangan" name="keterangan" value="" readonly></textarea>
                            
                        </div>  
                                            
                    </div>      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="nama">Identitas</small>
                            <input type="text" class="form-control form-control-sm" id="tipe" name="tipe" placeholder="KTP" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">Nomor Identitas</small>
                            <input type="text" class="form-control form-control-sm" id="identitas" name="identitas" placeholder="3711888200001" readonly>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="nama">Alamat</small>
                            <input type="text" class="form-control form-control-sm" id="alamat" name="alamat" placeholder="alamat" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">Desa</small>
                            <input type="text" class="form-control form-control-sm" id="desa" name="desa" placeholder="desa" readonly>
                        </div>                        
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">Kecamatan</small>
                            <input type="text" class="form-control form-control-sm" id="kecamatan" name="kecamatan" placeholder="prov" readonly>
                        </div>                        
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="nama">Kota/Kab</small>
                            <input type="text" class="form-control form-control-sm" id="kota" name="kota" placeholder="kota" readonly>
                        </div>                        
                    </div>    
                    <div class="form-row">
                        
                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">Provinsi</small>
                            <input type="text" class="form-control form-control-sm" id="prov" name="prov" placeholder="prov" readonly>
                        </div>  

                        <div class="form-group col-md-6">
                            <small class="font-weight-bold" for="telp">Perusahaan</small>
                            <input type="text" class="form-control form-control-sm" id="perusahaan" name="perusahaan" placeholder="perusahaan" readonly>
                        </div>  
                        
                    </div> 
                    <div class="form-row">
                        
                    <div class="form-group col-md-6">
                                <small class="font-weight-bold" for="bulan">Tagihan Bulan</small>
                                <select id="inputbulan" class="form-control form-control-sm" name="bulan" required>
                                <option selected="selected" disabled="true"></option>
                                    <?php 
                                        foreach($bulan as $b =>$bval){
                                            echo '<option value="'.$b.'" >'.$bval.'</option>';
                                        }                                 
                                    ?>                                
                                </select>                                
                                <span class="help-block"></span>
                            </div>                      
                       
                                      
                    </div>                     
                        
                 
            </div>

            <div class="modal-body bg-light">   
                <div class="text-center"> <strong class="text-primary">Tarif Layanan</strong> </div> 
             
                                    
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Meter Awal </small>
                            <div class="col-md-8">
                            <input type="text" class="form-control form-control-sm" id="meter_awal" name="meter_awal" placeholder="30000" readonly>
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Meter Akhir </small>
                            <div class="col-md-8">
                            <input type="text" class="form-control form-control-sm" id="meter_akhir" name="meter_akhir" placeholder="35000" onkeyup="update_tarif()" required>
                            </div>
                            
                </div>
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Rusak</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm " id="rusak" name="rusak" value="0"  onkeyup="update_tarif()" required>
                               
                                
                            </div>
                            
                </div>      
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Free Copy </small>
                            <div class="col-md-8">
                            <input type="text" class="form-control form-control-sm" id="free_copy" name="free_copy" placeholder="30000" readonly>
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif_ekstra">Tarif Tambahan </small>
                            <div class="col-md-8">
                            <input type="text" class="form-control form-control-sm" id="tarif_ekstra_value" name="tarif_ekstra_value" placeholder="30000" readonly>
                            <input type="hidden"  id="tarif_ekstra" name="tarif_ekstra" >
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Tarif </small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="tarif" name="tarif" value="aaa" readonly>
                                <input type="hidden" class="form-control form-control-sm" id="tarifvalue" name="tarifvalue" value="aaa" readonly>
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Tarif Copy</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="tarifcopy" name="tarifcopy" value="aaa" readonly>
                                <input type="hidden" class="form-control form-control-sm" id="tarifcopyvalue" name="tarifcopyvalue" value="aaa" readonly>
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Tambahan</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="totaltarifekstra" name="totaltarifekstra" value="aaa" readonly>
                                
                            </div>
                            
                </div>
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">ppn (%)</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="ppn" name="ppn" value="0" readonly>
                                
                            </div>
                            
                </div>      
             
             
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Total</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="total" name="total" value="0" readonly>
                                
                            </div>
                            
                </div>   
                <div class="form-group row">
                            <small class="font-weight-bold col-md-4 " for="tarif">Transport</small>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-control-sm text-right" id="transportasi" name="transportasi" value="0" readonly>
                                <input type="hidden" id="transportasival" name="transportasival" value="0" >
                                
                            </div>
                            
                </div>   
                
            </div>
            <div class="modal-footer">
                <button type="submit"   name="submit" class="btn btn-primary">Buat Tagihan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">tutup</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_invoice_preview" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content form">
            <div class="modal-header">                
                <h4 class="modal-title text-center">NOTA TAGIHAN </h4> 
                      
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
             <input type="hidden" id="invoiceid">
            
            <div class="modal-body bg-white">   
                <div id="invoice" class="container inv-main pt-0 mt-0">
                 <div class="inv-head row row-eq-height pt-0 mt-0">
                    <div class="col-4">
                                                                                      
                            <h5 class="my-0 py-0 font-weight-bold"><?=$setting['nama'];?></h5>                  
                            <div class="info-cv text-left">
                            <p class="my-0 py-0"><?=$setting['alamat'];?></p>                        
                            <p class="my-0 py-0"><?=$setting['kota'];?>, <span ><?=$setting['kodepos'];?></span></p>
                            <p class="my-0 py-0"><?=$setting['provinsi'];?>, <?=$setting['telp'];?></p>
                             
                            </div>
                    </div>
                    <div class="col-4">
                        <h4 class="border py-0 my-0 text-center">FAKTUR TAGIHAN</h4>
                    </div>
                    
                    <div class="col text-center position-relative">                                                 
                        <h4 class="border float-right d-inline px-2 py-0 my-0" id="inv-bulan">JUNI</h4>
                        <div  class="m-0 p-0 position-absolute" style="bottom:0; left:0; right:0; margin-left: auto; margin-right: auto; text-align: center; ">Faktur <span class="font-weight-bold" id="inv-no">INV/2021/06/0001</span></div>    
                    </div>
                 </div>
                 
                 
                 <div class="inv-subhead row align-items-start border">
                    <div class="col-8">
                        <table>                            
                            <tr class="my-0 py-0 text-capitalize "> 
                                <td class="align-top">Nama </td>
                                <td class="align-top">:</td>
                                <td class="pl-1 align-top"><span id="inv-perusahaan"></span>   </td></tr>                            
                            <tr class="my-0 py-0 text-capitalize align-top"> 
                                <td class="align-top">Alamat </td>
                                <td class="align-top">:</td>
                                <td id="inv-alamat" class="pl-1 align-top" >Mertojoyo Blok E3 ,Malang 651414</td></tr>
                            <tr class="my-0 py-0 text-capitalize "> 
                                <td colspan="2"></td>
                                <td class="pl-1 align-top"> <span id="inv-prov">Jawa Timur,  </span> <span id="inv-telp">081358887147</span></td></tr>
                            <tr class="my-0 py-0 "> 
                                <td  class="align-top">Model Mesin</td>
                                <td class="align-top">:</td>
                                <td id="inv-seri" class="pl-1 align-top">Ir 3223</td></tr>
                        </table>
                         
                    </div>
                    <div class="col-4 text-center">
                        <p id="inv-tanggal" class="my-0 py-0">08, Juni 2021</p>                      
                         <p>nopel <b id="inv-nopel">00000011</b> </p>                  
                     
                    </div>
                 </div> 
                 <div class="inv-detail row border">
                        <table class="table table-sm table-borderless my-0 py-0">
                            <thead class="border-bottom">
                                <tr class="font-weight-bold text-center">                                     
                                    <th  class="border-right">Barang</th>
                                    <th  class="border-right ">Unit</th>
                                    <th  class="border-right">Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="my-0 py-0" id="inv-tarif" >
                                    <td class="font-weight-bold border-right">Biaya Sewa</td>
                                    <td class="border-right text-center">1</td>
                                    <td class="border-right inv-tarif-sewa" >Rp. 100.000,00</td>
                                    <td class="inv-tarif-sewa text-right">Rp. 100.000,00</td>                                    
                                </tr>
                                <tr class="my-0 py-0" id="inv-minim">
                                    <td class="font-weight-bold border-right">Biaya Minimum Copy (<span class="inv-copy">3000</span> lbr)</td>
                                    <td class="border-right text-center">1</td>
                                    <td class="border-right inv-tarif-minim">Rp. 200.000,00</td>
                                    <td class="inv-tarif-minim text-right">Rp. 200.000,00</td>                                    
                                </tr >
                                                           
                                <tr class="my-0 py-0">
                                    <td  class="border-right">
                                       <span class="font-weight-bold"> Biaya Tambahan </span>
                                       <ol class="my-0 py-0">
                                        <li ><div class="row"> 
                                                <span class="col-3">Min Copy</span>  <span class="col">: <span class="inv-copy">3000</span></span>
                                                <span class="col-3">Meter Awal</span>  <span class="col">: <span class="inv-meter-lama">50</span></span>
                                        </div></li>
                                        <li ><div class="row"> 
                                                <span class="col-3">Rusak</span>  <span class="col">: <span class="inv-rusak">50</span></span>
                                                <span class="col-3">Meter Akhir</span>  <span class="col">: <span class="inv-meter-baru">50</span></span>
                                        
                                        </div></li>
                                        <li ><div class="row"> <span class="col-3">Tambahan</span>  <span class="col">: <span class="inv-tambahan">50</span></span></div></li>
                                        <li ><div class="row"> <span class="col-3">Tarif</span>  <span class="col-3">: <span class="inv-tarif-eks">50</span></span></div></li>
                                                                                
                                        </ol>

                                    </td>
                                    <td class="border-right"> </td>
                                    <td class="border-right"></td> 
                                    <td class="inv-tarif-tambahan text-right">Rp. 200.000,00</td>                                    
                                </tr>                                                           
                                </tbody>
                                <tfoot >
                                    <tr class="border-top  ">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> SubTotal </td>                                            
                                                                    
                                        <td class="inv-subtotal text-right">Rp. 200.000,00</td>                              
                                    </tr>
                                    <tr class="border-top  ">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> PPn </td>                                            
                                                                   
                                        <td class="inv-ppn text-right">Rp. 200.000,00</td>                         
                                    </tr>
                                    <tr class="border-top  border-bottom">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> Total </td>                                            
                                                                     
                                        <td class="inv-total text-right">Rp. 200.000,00</td>                             
                                    </tr>
                                </tfoot>
                        </table>                         
                        <div class="inv-note col-6">
                                    <!-- <u>Catatan :</u>                                    
                                    <p class="py-0 my-0" id="inv-keterangan"></p> -->
                                </div>

                                <div class="col-6 text-center">
                                    <p class="mb-3">Hormat Kami,</p>
                                    <p class="mt-2 pt-2 mb-0"><?=$setting['ttd'];?></p>
                                </div>
                 </div>
                </div>
            </div>   
                
            
            <div class="modal-footer">
                <button type="button"   name="submit" class="btn btn-outline-info" onclick="print_invoice()"><i class="fas fa-print"></i></button>
                 
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_invoice_transport_preview" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content form">
            <div class="modal-header">                
                <h4 class="modal-title text-center">FAKTUR TRANSPORT </h4> 
                      
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
            <input type="hidden" id="invoiceid_t">
            
            <div class="modal-body bg-white ">   
                <div id="invoice-transport" class="container inv-main pt-0 mt-0">
                 <div class="inv-head row row-eq-height pt-0 mt-0">
                    <div class="col-4">
                                                                                      
                            <h5 class="my-0 py-0 font-weight-bold"><?=$setting['nama'];?></h5>                  
                            <div class="info-cv text-left">
                            <p class="my-0 py-0"><?=$setting['alamat'];?></p>                        
                            <p class="my-0 py-0"><?=$setting['kota'];?>, <span ><?=$setting['kodepos'];?></span></p>
                            <p class="my-0 py-0"><?=$setting['provinsi'];?>, <?=$setting['telp'];?></p>
                             
                            </div>
                    </div>
                    <div class="col-4">
                        <h4 class="border py-0 my-0 text-center">FAKTUR TAGIHAN</h4>
                    </div>
                    
                    <div class="col text-center position-relative">                                                 
                        <h4 class="border float-right d-inline px-2 py-0 my-0" id="inv-bulan_t">JUNI</h4>
                        <div  class="m-0 p-0 position-absolute" style="bottom:0; left:0; right:0; margin-left: auto; margin-right: auto; text-align: center; ">Faktur <span class="font-weight-bold" id="inv-no_t">INV/2021/06/0001</span></div>    
                    </div>
                 </div>
                 
                 
                 <div class="inv-subhead row border">
                    <div class="col-8">
                        <table>                            
                            <tr class="my-0 py-0 text-capitalize "> 
                                    <td class="align-top">Nama </td>
                                    <td class="align-top">:</td>
                                    <td class="align-top pl-1"><span id="inv-perusahaan_t"></span>   </td></tr>                            
                            <tr class="my-0 py-0 text-capitalize"> 
                                    <td class="align-top">Alamat </td>
                                    <td class="align-top">:</td>
                                    <td id="inv-alamat_t"  class="align-top pl-1">Mertojoyo Blok E3 ,Malang 651414</td></tr>
                            <tr class="my-0 py-0 text-capitalize"> 
                                    <td colspan="2"></td>
                                    <td class="align-top pl-1"> <span id="inv-prov_t">Jawa Timur,  </span> <span id="inv-telp_t">081358887147</span></td></tr>
                            <tr class="my-0 py-0 "> 
                                    <td class="align-top">Model Mesin</td>
                                    <td class="align-top">:</td>
                                    <td id="inv-seri_t" class="align-top pl-1">Ir 3223</td></tr>
                        </table>
                         
                    </div>
                    <div class="col-4 text-center">
                        <p id="inv-tanggal_t" class="my-0 py-0">08, Juni 2021</p>                      
                         <p>nopel <b id="inv-nopel_t">00000011</b> </p>                  
                     
                    </div>
                 </div> 
                 <div class="inv-detail row border">
                        <table class="table table-sm table-borderless my-0 py-0">
                            <thead class="border-bottom">
                                <tr class="font-weight-bold text-center">                                     
                                    <th  class="border-right">Keterangan</th>
                                    
                                    <th  class="border-right">Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="my-0 py-0" id="inv-tarif" >
                                    <td class="font-weight-bold border-right">Biaya Transport Teknisi</td>
                                    
                                    <td class="border-right inv-tarif-transport" >Rp. 100.000,00</td>
                                    <td class="inv-tarif-transport text-right">Rp. 100.000,00</td>                                    
                                </tr>
                                 
                                                           
                                
                                             
                                </tbody>
                                <tfoot >
                                    <tr class="border-top  ">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> SubTotal </td>                                            
                                                                    
                                        <td class="inv-subtotal text-right">Rp. 200.000,00</td>                              
                                    </tr>
                                    <tr class="border-top  ">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> PPn </td>                                            
                                                                   
                                        <td class="inv-ppn text-right">Rp. 200.000,00</td>                         
                                    </tr>
                                    <tr class="border-top  border-bottom">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> Total </td>                                            
                                                                     
                                        <td class="inv-total text-right">Rp. 200.000,00</td>                             
                                    </tr>
                                </tfoot>
                        </table>                         
                                 <div class="inv-note col-6">
                                     
                                </div>

                                <div class="col-6 text-center">
                                    <p class="mb-3">Hormat Kami,</p>
                                    <p class="mt-2 pt-2 mb-0"><?=$setting['ttd'];?></p>
                                </div>
                 </div>
                </div>
            </div>   
            
            <div class="modal-footer">
                <button type="button"   name="submit" class="btn btn-outline-info" onclick="print_invoice_transport()"><i class="fas fa-print"></i></button>
                 
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  
$(document).ready(function() {

    $('#inputnama').autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?= base_url('app/data/customer')?>",
          
          data: {
            term: request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data );
          }
        } );
      },
      
      select: function( event, ui ) {
        $( "#inputnama" ).val( ui.item.label.split('|')[0] );
        $( "#idpelanggan" ).val( ui.item.value );
        return false;
      },
    focus: function(event, ui){
        $( "#inputnama" ).val( ui.item.label.split('|')[0] );
        $( "#idpelanggan" ).val( ui.item.value );
     return false;
   },
    });

    $('#inputcom').select2({
      theme: 'bootstrap4'
    });

    $('#n-inputcom').select2({
      theme: 'bootstrap4'
    });

    

});

    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url();?>/plugins/datatables/lang/Indonesian.json" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/service/ajaxdata')?>",
            
            columnDefs: [
                { targets: 5, className: 'text-center'}, //first column is not orderable.
                { targets: -1, className: 'text-center'}, //last column center.
                { targets: -2, className: 'text-center'}, //last column center.
                { targets: 0, className: 'text-center'}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: 'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: 'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                    messageTop :'Data Layanan Aktif <?php echo date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
        });
    });

function gantipelanggan(){
    $('#formlayanan').toggle();
    $('#formlayananpelangganbaru').toggle();
}    
    function add_service()
{
    save_method = 'add';
    resetform();
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Layanan Baru'); // Set Title to Bootstrap modal title
    

}
 function resetform(){

    $('#idservice').val(''); 
    $('#formlayanan')[0].reset(); // reset form on modals
     
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
 }

 function resetforminvoice(){

    $('#idservice').val(''); 
    $('#forminvoice')[0].reset(); // reset form on modals
 
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
}

 function invoice(serviceid,month)
{
     
    $('#bulanakhir').val(month);
    $('#idservice').val(serviceid); 
    $('#forminvoice')[0].reset(); // reset form on modals

    $.ajax({	
    method:'POST',
	url: url+"/app/data/service",
	data: {id : serviceid}
    })
	.done(function(data){        
		 console.log(data);        
        let x=JSON.parse(data)[0];
        console.log(x);
        $('#idservice').val(x.id_service);
        $('#nama').val(x.nama);
        $('#perusahaan').val(x.perusahaan);
        $('#alamat').val(x.alamat);
        $('#telp').val(x.telp);
        $('#seri').val(x.seri);
        $('#merk').val(x.merk);
        $('#tipe').val(x.jeniskartu);
        $('#identitas').val(x.nomorkartu);
        $('#kota').val(x.kota);
        $('#prov').val(x.provinsi);
        $('#desa').val(x.desa);
        $('#kecamatan').val(x.kecamatan);
        $('#meter_awal').val(x.meter_akhir);
        $('#free_copy').val(x.free_copy);
        $('#tarif').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(x.tarif));         
        $('#tarifvalue').val(x.tarif);
        $('#tarifcopy').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(x.tarif_copy)); 
        $('#tarifcopyvalue').val(x.tarif_copy);
        $('#totaltarifekstra').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(0)); 
        $('#tarif_ekstra_value').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(x.tarif_ekstra)); 
        let a =parseInt(x.tarif) + parseInt(x.tarif_copy);
            a = a + (a*x.ppn / 100);
            
        $('#total').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(a)); 
        $('#tarif_ekstra').val(x.tarif_ekstra);
        $('#ppn').val(x.ppn);
        $('#keterangan').val(x.keterangan);
        $('#transportasi').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(x.transportasi));
        $('#transportasival').val(x.transportasi);


		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
    $('#modal_invoice').modal('show'); // show bootstrap modal
    $('.modal-title').text('Buat Tagihan'); // Set Title to Bootstrap modal title
    

}
function print_invoice(){
    let i =$('#inv-no').html(); 
    $("#invoice").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: i,
        	doctype: '<!doctype html>'
	});
}

function print_invoice_transport(){
    let i =$('#inv-no_t').html(); 
    $("#invoice-transport").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: i,
        	doctype: '<!doctype html>'
	});
}
function invoice_preview(invoiceid)
{
    $('#modal_invoice_preview').modal('show'); // show bootstrap modal
      
    $('#invoiceid').val(invoiceid); 
    
    
    $.ajax({	
    method:'POST',
	url: url+"/app/data/invoice",
	data: {id : invoiceid}
    })
	.done(function(data){        
		// console.log(data);        
        let x=JSON.parse(data);
        //console.log(x);

        let money = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 2
        })

        let bb = [ 'JANUARI',
                   'FEBRUARI',
                   'MARET',
                   'APRIL',
                   'MEI',
                   'JUNI',
                   'JULI',
                   'AGUSTUS',
                   'SEPTEMBER',
                   'OKTOBER',
                   'NOPEMBER',
                   'DESEMBER'                
                ];            
        $('#inv-bulan').html(bb[x.bulan - 1]);
        let d=new Date(Date.parse(x.created_at));         
        $('#inv-no').html('INV/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4));
        $('#inv-nama').html(x.nama);
        $('#inv-perusahaan').html(x.perusahaan);
        if(!x.perusahaan){
           $('#inv-perusahaan').html(x.nama);
            
        }
         
        $('#inv-alamat').html(x.alamat);
        $('#inv-prov').html(x.kota+', '+x.provinsi+', ');
        $('#inv-nopel').html(pad(x.customer_id,5));
        $('#inv-telp').html(x.telp);
        $('#inv-seri').html(x.seri);        
        let month = d.toLocaleString('id-ID', { month: 'long' });
         
        $('#inv-tanggal').html(d.getDate()+' '+month+' '+d.getFullYear());
        $('.inv-tarif-sewa').html(money.format(x.tarif_sewa));
        $('.inv-tarif-minim').html(money.format(x.tarif_copy));
        if(parseInt(x.tarif_sewa) > 0)
            $('#inv-tarif').show();
        else
            $('#inv-tarif').hide();

        if(parseInt(x.tarif_copy) > 0)
            $('#inv-minim').show();
        else
            $('#inv-minim').hide();
         
        
        $('.inv-copy').html(x.free_copy);
        $('.inv-rusak').html(x.rusak);
        let t= parseInt(x.meter_akhir) - parseInt(x.meter_awal) - parseInt(x.rusak);
        let tambahan = 0;
        
            if(parseInt(t) > parseInt(x.free_copy)){
                tambahan = parseInt(t) - parseInt(x.free_copy);
                 
            }
             
        $('.inv-tambahan').html(tambahan);
        $('.inv-tarif-eks').html(x.tarif_ekstra);
        $('.inv-tarif-tambahan').html( money.format(tambahan * parseInt(x.tarif_ekstra)) );
            let subtotal = 0;
            if(parseInt(t) > parseInt(x.free_copy)){
                
                subtotal = parseInt(x.tarif_sewa) + parseInt(x.tarif_copy) + (tambahan * parseInt(x.tarif_ekstra));      
                 
            }
            else{
                subtotal = parseInt(x.tarif_sewa) + parseInt(x.tarif_copy);
                
            }
            
        $('.inv-subtotal').html(money.format(subtotal));
            let ppn = subtotal * parseFloat(x.ppn) / 100;
            
        $('.inv-ppn').html(money.format(ppn));
        $('.inv-total').html(money.format(subtotal + ppn));
        $('.inv-meter-lama').html(x.meter_awal);
        $('.inv-meter-baru').html(x.meter_akhir);

        $('#inv-keterangan').html(x.keterangan);

        $('.modal-title').text('Faktur ' +'INV/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4)); // Set Title to Bootstrap modal title 
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
     
    

}

function invoice_transport_preview(invoiceid)
{
    $('#modal_invoice_transport_preview').modal('show'); // show bootstrap modal
      
    $('#invoiceid_t').val(invoiceid); 
    
    
    $.ajax({	
    method:'POST',
	url: url+"/app/data/invoice",
	data: {id : invoiceid}
    })
	.done(function(data){        
		// console.log(data);        
        let x=JSON.parse(data);
        //console.log(x);

        let money = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 2
        })

        let bb = [ 'JANUARI',
                   'FEBRUARI',
                   'MARET',
                   'APRIL',
                   'MEI',
                   'JUNI',
                   'JULI',
                   'AGUSTUS',
                   'SEPTEMBER',
                   'OKTOBER',
                   'NOPEMBER',
                   'DESEMBER'                
                ];            
        $('#inv-bulan_t').html(bb[x.bulan - 1]);
        let d=new Date(Date.parse(x.created_at));         
        $('#inv-no_t').html('TRS/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4));
        $('#inv-nama_t').html(x.nama);
        $('#inv-perusahaan_t').html(x.perusahaan);
        if(!x.perusahaan)
        $('#inv-perusahaan_t').html(x.nama);
        $('#inv-alamat_t').html(x.alamat);
        $('#inv-prov_t').html(x.kota+', '+x.provinsi+', ');
        $('#inv-nopel_t').html(pad(x.customer_id,5));
        $('#inv-telp_t').html(x.telp);
        $('#inv-seri_t').html(x.seri);        
        let month = d.toLocaleString('id-ID', { month: 'long' });
         
        $('#inv-tanggal_t').html(d.getDate()+' '+month+' '+d.getFullYear());
        $('.inv-tarif-transport').html(money.format(x.transportasi));
        
           
            
        $('.inv-subtotal').html(money.format(x.transportasi));
            
        $('.inv-ppn').html(money.format(0));
        $('.inv-total').html(money.format(x.transportasi));       
 

        $('.modal-title').text('Faktur Transport' +'TRS/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4)); // Set Title to Bootstrap modal title 
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
     
    

}


function update_tarif(){
    
    let tarif_ekstra =parseInt($('#tarif_ekstra').val());
    let tarif =parseInt($('#tarifvalue').val());
    let tarif_copy =parseInt($('#tarifcopyvalue').val());
    let free_copy =parseInt($('#free_copy').val());
    let meter_awal =$('#meter_awal').val();
    let meter_akhir =$('#meter_akhir').val();

    if(meter_akhir.length < meter_awal.length )
        return false;
    else if(parseInt(meter_akhir) < parseInt(meter_awal)){
        alert('Inputan tidak boleh kurang dari meter awal');
        return false;
    }
    let rusak = parseInt($('#rusak').val());
    let ppn = parseInt($('#ppn').val());
    let total = (parseInt(meter_akhir) - parseInt(meter_awal) - rusak);
    let total_all=0;
    if(free_copy >=total){
      total_all= tarif + tarif_copy ;
      $('#totaltarifekstra').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(0)); 
    }
    else
     {   
         let eks =(total - free_copy)*tarif_ekstra ;
         total_all = tarif+tarif_copy + eks;
         
         $('#totaltarifekstra').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(eks)); 

     }
     total_all= total_all + (total_all * ppn / 100) ; 
     
    
    $('#total').val(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(total_all)); 

}
function delete_service(idlayanan){
    $('#idlayanan').val(idlayanan);      
    $('#idhapus').val(idlayanan);      
    $('#modal-delete').modal('show'); // show bootstrap modal
}

$('#forminvoice').submit(function(e){
    e.preventDefault();
    let d = new Date();
    let n = d.getMonth() + 1;
    let b= $('#bulanakhir').val();
    console.log( n+'   a '+b);
    if(parseInt(n)==parseInt(b)){
        var agree=confirm("Tagihan terakhir telah dibuat bulan ini, apa Anda yakin akan membuat tagihan yang baru?");
        if (!agree){
                resetforminvoice();
                $('#modal_invoice').modal('hide');
                reload_table();
            return false ;        
        }
    }    
    
        let id = $('#idservice').val();
        let nama = $('#nama').val();
        let perusahaan = $('#perusahaan').val();
        let alamat = $('#alamat').val();
        let telp = $('#telp').val();
        let seri = $('#seri').val();
        let merk = $('#merk').val();
        let tipe = $('#tipe').val();
        let identitas = $('#identitas').val();
        let kota =$('#kota').val();
        let prov = $('#prov').val();
        let desa = $('#desa').val();
        let kec = $('#kecamatan').val();
        let meter_awal = $('#meter_awal').val();
        let meter_akhir = $('#meter_akhir').val();
        let free_copy = $('#free_copy').val();
        let tarif = $('#tarifvalue').val();
        let tarif_copy = $('#tarifcopyvalue').val();
        let tarif_ekstra = $('#tarif_ekstra').val();
        let rusak = $('#rusak').val();
        let ppn = $('#ppn').val();
        let bulan = $('#inputbulan').val();
        let keterangan = $('#keterangan').val();
        let transport = $('#transportasival').val();

        
        let invoice ={  id :id,
                        nama: nama,
                        perusahaan: perusahaan,
                        alamat: alamat,
                        telp : telp,
                        seri : seri,
                        merk : merk,
                        tipe : tipe,
                        identitas : identitas,
                        kota : kota,
                        prov : prov,
                        desa : desa,
                        kec : kec,
                        meter_awal : meter_awal,
                        meter_akhir : meter_akhir,
                        free_copy : free_copy,
                        tarif : tarif,
                        tarif_copy : tarif_copy,
                        tarif_ekstra : tarif_ekstra,
                        rusak : rusak,
                        ppn:ppn,
                        bulan:bulan,
                        keterangan:keterangan ,
                        transportasi:transport
                           };
    
   
         
         

        $.ajax({	
        url: url+"/panel/invoices/new",
        data: invoice,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetforminvoice();
                $('#modal_invoice').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});

$('#formlayanan').submit(function(e){
    e.preventDefault();
    console.log('submit dimulai');
    let id=$('#id_service').val();
    let idpelanggan=$('#idpelanggan').val();
    let com=$('#inputcom').val();
    let meter=$('#inputmeter').val();    
    let keterangan= $('#inputketerangan').val();
    let tarif=$('#inputtarif').val();
    let tarif_eks= $('#inputtarifekstra').val();
    let tarif_copy= $('#inputtarifcopy').val();
    let freecopy= $('#inputcopy').val();
    let ppn= $('#inputppn').val();
    let transport= $('#inputtransport').val();
   
        // add new pelanggan
        let data = { 
                id :id,
                customer : idpelanggan ,
                com : com,
                meter: meter,              
                tarif: tarif,
                tarif_eks: tarif_eks,
                tarif_copy: tarif_copy,
                freecopy: freecopy,  
                ppn: ppn,  
                keterangan : keterangan,
                transportasi : transport,
               };
         

        $.ajax({	
        url: url+"/panel/service/new",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetform();
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});

$('#formlayananpelangganbaru').submit(function(e){
    e.preventDefault();
    console.log('submit dimulai');
    let id=$('#n-id_service').val();     
    let com=$('#n-inputcom').val();
    let meter=$('#n-inputmeter').val();    
    let keterangan= $('#n-inputketerangan').val();
    let tarif=$('#n-inputtarif').val();
    let tarif_eks= $('#n-inputtarifekstra').val();
    let tarif_copy= $('#n-inputtarifcopy').val();
    let freecopy= $('#n-inputcopy').val();
    let ppn= $('#n-inputppn').val();
    let transport= $('#n-inputtransport').val();
   

    // data pelanggan
    let nama=$('#n-inputnama').val();
    let perusahaan=$('#n-inputperusahaan').val();
    let iden=$('#n-inputidentitas').val();
    let no_iden=$('#n-inputnomor').val();
    let alamat= $('#n-inputalamat').val();
    let provinsi = $('#n-inputprovinsi').val();
    let kota = $('#n-kota').val();
    let kec = $('#n-kec').val();
    let desa = $('#n-desa').val();
    let telp = $('#inputtelp').val();

        // add new pelanggan
        let data = { 
                nama:nama,
                perusahaan:perusahaan,
                identitas:iden,
                no_identitas:no_iden,
                alamat:alamat,
                provinsi:provinsi,
                kota:kota,
                kec:kec,
                desa:desa,
                telp:telp,

                id :id,                
                com : com,
                meter: meter,              
                tarif: tarif,
                tarif_eks: tarif_eks,
                tarif_copy: tarif_copy,
                freecopy: freecopy,  
                ppn: ppn,  
                keterangan : keterangan,
                transportasi : transport,
               };
         
                
        $.ajax({	
        url: url+"/panel/service/newcs",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            //console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                alert( d.pesan+" ("+d.id+")" );   
                resetform();
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});
function edit_service(idservice)
{
        
    resetform();
    $('#id_service').val(idservice); 
    
    $.ajax({	
	url: url+"/panel/service/getdata/"+idservice	
    })
	.done(function(data){        
	     

     

     let service = JSON.parse(data);
     $('#idpelanggan').val(service.data.customer_id);
     $('#inputmeter').val(service.data.meter_akhir);
     $('#inputketerangan').val(service.data.keterangan);
     $('#inputtarif').val(service.data.tarif);
     $('#inputtarifekstra').val(service.data.tarif_ekstra);
     $('#inputtarifcopy').val(service.data.tarif_copy);
     $('#inputcopy').val(service.data.free_copy);
     $('#inputppn').val(service.data.ppn);
     $('#inputnama').val(service.data.nama);
     $('#inputtransport').val(service.data.transportasi);
     $('#inputcom').val(service.data.com_id).trigger('change');

     
          
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Edit Data Mesin'); // Set Title to Bootstrap modal title
	})
    .fail(function(xhr,status,error) {
        alert( "error "+error);
        console.log(error);
    })
    .always(function(){
        
    });
    

}

$('#formhapus').submit(function(e){
    e.preventDefault();
    console.log('hapus dimulai');
    let id=$('#idhapus').val();
    
        // add new pelanggan
        let data = { 
                id :id               
               };
         

        $.ajax({	
        url: url+"/panel/service/delete",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                console.log(data);
                alert( d.pesan );   
                $('#idhapus').val('');                
                $('#modal-delete').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});




    function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
    </script>
<?= $this->endSection() ?>

