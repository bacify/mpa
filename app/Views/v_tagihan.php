<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        
        <button class="btn btn-default btn-sm mb-2" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
        <hr>
        <table id="table" class="table table-striped table-bordered table-sm " cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Faktur</th>                  
                    <th>Kontrak</th>                  
                    <th>bulan</th>                  
                    <th>Pelanggan</th>                  
                                      
                    <th>Meter</th>                    
                    <th>Total Tagihan</th>                                       
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <th colspan="7" style="text-align:right !important;">Total:</th>
                <th colspan="2" style="text-align:left !important;"></th>
                
            </tr>
        </tfoot>
        </table>
   

<div class="modal" id="modal-delete" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" id="formhapus" class="form-horizontal" method="POST">
      <div class="modal-body">
        <p>Apakah Anda yakin akan menghapus Layanan <b id="idtagihan" > Ini </b> ?</p>
        <input type="hidden" id="idhapus" value="">
      </div>
      <div class="modal-footer">
        <button type="submit" id="btnDelete" class="btn btn-primary" name="submit" >Hapus Mesin</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_invoice_preview" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content form">
            <div class="modal-header">                
                <h4 class="modal-title text-center">NOTA TAGIHAN </h4> 
                      
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
             <input type="hidden" id="invoiceid">
            
            <div class="modal-body bg-white">   
                <div id="invoice" class="container inv-main pt-0 mt-0">
                 <div class="inv-head row row-eq-height pt-0 mt-0">
                    <div class="col-4">
                                                                                      
                            <h5 class="my-0 py-0 font-weight-bold"><?=$setting['nama'];?></h5>                  
                            <div class="info-cv text-left">
                            <p class="my-0 py-0"><?=$setting['alamat'];?></p>                        
                            <p class="my-0 py-0"><?=$setting['kota'];?>, <span ><?=$setting['kodepos'];?></span></p>
                            <p class="my-0 py-0"><?=$setting['provinsi'];?>, <?=$setting['telp'];?></p>
                             
                            </div>
                    </div>
                    <div class="col-4">
                        <h4 class="border py-0 my-0 text-center">FAKTUR TAGIHAN</h4>
                    </div>
                    
                    <div class="col text-center position-relative">                                                 
                        <h4 class="border float-right d-inline px-2 py-0 my-0" id="inv-bulan">JUNI</h4>
                        <div  class="m-0 p-0 position-absolute" style="bottom:0; left:0; right:0; margin-left: auto; margin-right: auto; text-align: center; ">Faktur <span class="font-weight-bold" id="inv-no">INV/2021/06/0001</span></div>    
                    </div>
                 </div>
                 
                 
                 <div class="inv-subhead row align-items-start border">
                    <div class="col-8">
                        <table>                            
                            <tr class="my-0 py-0 text-capitalize "> 
                                <td class="align-top">Nama </td>
                                <td class="align-top">:</td>
                                <td class="pl-1 align-top"><span id="inv-perusahaan"></span>   </td></tr>                            
                            <tr class="my-0 py-0 text-capitalize align-top"> 
                                <td class="align-top">Alamat </td>
                                <td class="align-top">:</td>
                                <td id="inv-alamat" class="pl-1 align-top" >Mertojoyo Blok E3 ,Malang 651414</td></tr>
                            <tr class="my-0 py-0 text-capitalize "> 
                                <td colspan="2"></td>
                                <td class="pl-1 align-top"> <span id="inv-prov">Jawa Timur,  </span> <span id="inv-telp">081358887147</span></td></tr>
                            <tr class="my-0 py-0 "> 
                                <td  class="align-top">Model Mesin</td>
                                <td class="align-top">:</td>
                                <td id="inv-seri" class="pl-1 align-top">Ir 3223</td></tr>
                        </table>
                         
                    </div>
                    <div class="col-4 text-center">
                        <p id="inv-tanggal" class="my-0 py-0">08, Juni 2021</p>                      
                         <p>nopel <b id="inv-nopel">00000011</b> </p>                  
                     
                    </div>
                 </div> 
                 <div class="inv-detail row border">
                        <table class="table table-sm table-borderless my-0 py-0">
                            <thead class="border-bottom">
                                <tr class="font-weight-bold text-center">                                     
                                    <th  class="border-right">Barang</th>
                                    <th  class="border-right ">Unit</th>
                                    <th  class="border-right">Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="my-0 py-0" id="inv-tarif" >
                                    <td class="font-weight-bold border-right">Biaya Sewa</td>
                                    <td class="border-right text-center">1</td>
                                    <td class="border-right inv-tarif-sewa" >Rp. 100.000,00</td>
                                    <td class="inv-tarif-sewa text-right">Rp. 100.000,00</td>                                    
                                </tr>
                                <tr class="my-0 py-0" id="inv-minim">
                                    <td class="font-weight-bold border-right">Biaya Minimum Copy (<span class="inv-copy">3000</span> lbr)</td>
                                    <td class="border-right text-center">1</td>
                                    <td class="border-right inv-tarif-minim">Rp. 200.000,00</td>
                                    <td class="inv-tarif-minim text-right">Rp. 200.000,00</td>                                    
                                </tr >
                                                           
                                <tr class="my-0 py-0">
                                    <td  class="border-right">
                                       <span class="font-weight-bold"> Biaya Tambahan </span>
                                       <ol class="my-0 py-0">
                                        <li ><div class="row"> 
                                                <span class="col-3">Min Copy</span>  <span class="col">: <span class="inv-copy">3000</span></span>
                                                <span class="col-3">Meter Awal</span>  <span class="col">: <span class="inv-meter-lama">50</span></span>
                                        </div></li>
                                        <li ><div class="row"> 
                                                <span class="col-3">Rusak</span>  <span class="col">: <span class="inv-rusak">50</span></span>
                                                <span class="col-3">Meter Akhir</span>  <span class="col">: <span class="inv-meter-baru">50</span></span>
                                        
                                        </div></li>
                                        <li ><div class="row"> <span class="col-3">Tambahan</span>  <span class="col">: <span class="inv-tambahan">50</span></span></div></li>
                                        <li ><div class="row"> <span class="col-3">Tarif</span>  <span class="col-3">: <span class="inv-tarif-eks">50</span></span></div></li>
                                                                                
                                        </ol>

                                    </td>
                                    <td class="border-right"> </td>
                                    <td class="border-right"></td> 
                                    <td class="inv-tarif-tambahan text-right">Rp. 200.000,00</td>                                    
                                </tr>                                                           
                                </tbody>
                                <tfoot >
                                    <tr class="border-top  ">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> SubTotal </td>                                            
                                                                    
                                        <td class="inv-subtotal text-right">Rp. 200.000,00</td>                              
                                    </tr>
                                    <tr class="border-top  ">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> PPn </td>                                            
                                                                   
                                        <td class="inv-ppn text-right">Rp. 200.000,00</td>                         
                                    </tr>
                                    <tr class="border-top  border-bottom">
                                        <td colspan="3" class="text-right font-weight-bold border-right"> Total </td>                                            
                                                                     
                                        <td class="inv-total text-right">Rp. 200.000,00</td>                             
                                    </tr>
                                </tfoot>
                        </table>                         
                        <div class="inv-note col-6">
                                    <!-- <u>Catatan :</u>                                    
                                    <p class="py-0 my-0" id="inv-keterangan"></p> -->
                                </div>

                                <div class="col-6 text-center">
                                    <p class="mb-3">Hormat Kami,</p>
                                    <p class="mt-2 pt-2 mb-0"><?=$setting['ttd'];?></p>
                                </div>
                 </div>
                </div>
            </div>   
                
            
            <div class="modal-footer">
                <button type="button"   name="submit" class="btn btn-outline-info" onclick="print_invoice()"><i class="fas fa-print"></i></button>
                 
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_invoice_transport_preview" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content form">
            <div class="modal-header">                
                <h4 class="modal-title text-center">FAKTUR TRANSPORT </h4> 
                      
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>            
            <input type="hidden" id="invoiceid_t">
            
            <div class="modal-body bg-white ">   
                <div id="invoice-transport" class="container inv-main pt-0 mt-0">
                 <div class="inv-head row row-eq-height pt-0 mt-0">
                    <div class="col-4">
                                                                                      
                            <h5 class="my-0 py-0 font-weight-bold"><?=$setting['nama'];?></h5>                  
                            <div class="info-cv text-left">
                            <p class="my-0 py-0"><?=$setting['alamat'];?></p>                        
                            <p class="my-0 py-0"><?=$setting['kota'];?>, <span ><?=$setting['kodepos'];?></span></p>
                            <p class="my-0 py-0"><?=$setting['provinsi'];?>, <?=$setting['telp'];?></p>
                             
                            </div>
                    </div>
                    <div class="col-4">
                        <h4 class="border py-0 my-0 text-center">FAKTUR TAGIHAN</h4>
                    </div>
                    
                    <div class="col text-center position-relative">                                                 
                        <h4 class="border float-right d-inline px-2 py-0 my-0" id="inv-bulan_t">JUNI</h4>
                        <div  class="m-0 p-0 position-absolute" style="bottom:0; left:0; right:0; margin-left: auto; margin-right: auto; text-align: center; ">Faktur <span class="font-weight-bold" id="inv-no_t">INV/2021/06/0001</span></div>    
                    </div>
                 </div>
                 
                 
                 <div class="inv-subhead row border">
                    <div class="col-8">
                        <table>                            
                            <tr class="my-0 py-0 text-capitalize "> 
                                    <td class="align-top">Nama </td>
                                    <td class="align-top">:</td>
                                    <td class="align-top pl-1"><span id="inv-perusahaan_t"></span>   </td></tr>                            
                            <tr class="my-0 py-0 text-capitalize"> 
                                    <td class="align-top">Alamat </td>
                                    <td class="align-top">:</td>
                                    <td id="inv-alamat_t"  class="align-top pl-1">Mertojoyo Blok E3 ,Malang 651414</td></tr>
                            <tr class="my-0 py-0 text-capitalize"> 
                                    <td colspan="2"></td>
                                    <td class="align-top pl-1"> <span id="inv-prov_t">Jawa Timur,  </span> <span id="inv-telp_t">081358887147</span></td></tr>
                            <tr class="my-0 py-0 "> 
                                    <td class="align-top">Model Mesin</td>
                                    <td class="align-top">:</td>
                                    <td id="inv-seri_t" class="align-top pl-1">Ir 3223</td></tr>
                        </table>
                         
                    </div>
                    <div class="col-4 text-center">
                        <p id="inv-tanggal_t" class="my-0 py-0">08, Juni 2021</p>                      
                         <p>nopel <b id="inv-nopel_t">00000011</b> </p>                  
                     
                    </div>
                 </div> 
                 <div class="inv-detail row border">
                        <table class="table table-sm table-borderless my-0 py-0">
                            <thead class="border-bottom">
                                <tr class="font-weight-bold text-center">                                     
                                    <th  class="border-right">Keterangan</th>
                                    
                                    <th  class="border-right">Harga</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="my-0 py-0" id="inv-tarif" >
                                    <td class="font-weight-bold border-right">Biaya Transport Teknisi</td>
                                    
                                    <td class="border-right inv-tarif-transport" >Rp. 100.000,00</td>
                                    <td class="inv-tarif-transport text-right">Rp. 100.000,00</td>                                    
                                </tr>
                                 
                                                           
                                
                                             
                                </tbody>
                                <tfoot >
                                    <tr class="border-top  ">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> SubTotal </td>                                            
                                                                    
                                        <td class="inv-subtotal text-right">Rp. 200.000,00</td>                              
                                    </tr>
                                    <tr class="border-top  ">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> PPn </td>                                            
                                                                   
                                        <td class="inv-ppn text-right">Rp. 200.000,00</td>                         
                                    </tr>
                                    <tr class="border-top  border-bottom">
                                        <td colspan="2" class="text-right font-weight-bold border-right"> Total </td>                                            
                                                                     
                                        <td class="inv-total text-right">Rp. 200.000,00</td>                             
                                    </tr>
                                </tfoot>
                        </table>                         
                                 <div class="inv-note col-6">
                                     
                                </div>

                                <div class="col-6 text-center">
                                    <p class="mb-3">Hormat Kami,</p>
                                    <p class="mt-2 pt-2 mb-0"><?=$setting['ttd'];?></p>
                                </div>
                 </div>
                </div>
            </div>   
            
            <div class="modal-footer">
                <button type="button"   name="submit" class="btn btn-outline-info" onclick="print_invoice_transport()"><i class="fas fa-print"></i></button>
                 
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('plugins/datatables/datatables.min.js');?>"></script>

<script type="text/javascript">  
$(document).ready(function() {

    
    

    

});

    $(document).ready(function() {
        table = $('#table').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url();?>/plugins/datatables/lang/Indonesian.json" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[1,'desc']], //init datatable not ordering
             
            ajax: "<?php echo site_url('panel/invoices/ajaxdata')?>",
            
            columnDefs: [
                { targets: 0, orderable: false}, //first column is not orderable.
                { targets: -1, className: 'text-center'}, //last column center.               
                { targets: 0, className: 'text-center'}, //last column center.
              
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: 'Faktur <?php echo date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: 'Faktur <?php echo date('d-m-Y'); ?> ',
                    messageTop :'Faktur <?php echo date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                let x=0;

                    if(typeof i === 'string'){
                         x = i.replace(/[Rp.]/g, '').replace(',','.') * 1;
                        
                    }else if(  typeof i === 'number'){
                        
                        x = i;
                    }else 
                    {
                        
                        x= 0;
                    }
                    
                return x;
            };
 
            // Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 7).footer() ).html(
                ' '+new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(pageTotal) +' ('+ new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(total) +' total)'
            );
        },
        });
    });

    function add_service()
{
    save_method = 'add';
    resetform();
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Layanan Baru'); // Set Title to Bootstrap modal title
    

}
 function resetform(){

    $('#idservice').val(''); 
    $('#formlayanan')[0].reset(); // reset form on modals
     
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
 }

 function resetforminvoice(){

    $('#idservice').val(''); 
    $('#forminvoice')[0].reset(); // reset form on modals
 
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
}

 

function invoice(invoiceid)
{
    $('#modal_invoice_preview').modal('show'); // show bootstrap modal
      
    $('#invoiceid').val(invoiceid); 
    
    
    $.ajax({	
    method:'POST',
	url: url+"/app/data/invoice",
	data: {id : invoiceid}
    })
	.done(function(data){        
		// console.log(data);        
        let x=JSON.parse(data);
        //console.log(x);

        let money = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 2
        })
        let bb = [ 'JANUARI',
                   'FEBRUARI',
                   'MARET',
                   'APRIL',
                   'MEI',
                   'JUNI',
                   'JULI',
                   'AGUSTUS',
                   'SEPTEMBER',
                   'OKTOBER',
                   'NOPEMBER',
                   'DESEMBER'                
                ];          

        $('#inv-bulan').html(bb[x.bulan -1]);
        let d=new Date(Date.parse(x.created_at));         
        $('#inv-no').html('INV/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4));
        $('#inv-nama').html(x.nama);
        $('#inv-perusahaan').html(x.perusahaan);
        if(!x.perusahaan)
        $('#inv-perusahaan').html(x.nama);
        $('#inv-alamat').html(x.alamat);
        $('#inv-prov').html(x.kota+', '+x.provinsi+', ');
        $('#inv-nopel').html(pad(x.customer_id,5));
        $('#inv-telp').html(x.telp);
        $('#inv-seri').html(x.seri);        
        let month = d.toLocaleString('id-ID', { month: 'long' });
         
        $('#inv-tanggal').html(d.getDate()+' '+month+' '+d.getFullYear());
        $('.inv-tarif-sewa').html(money.format(x.tarif_sewa));
        $('.inv-tarif-minim').html(money.format(x.tarif_copy));
        if(parseInt(x.tarif_sewa) > 0)
            $('#inv-tarif').show();
        else
            $('#inv-tarif').hide();

        if(parseInt(x.tarif_copy) > 0)
            $('#inv-minim').show();
        else
            $('#inv-minim').hide();
         
        
        $('.inv-copy').html(x.free_copy);
        $('.inv-rusak').html(x.rusak);
        let t= parseInt(x.meter_akhir) - parseInt(x.meter_awal) - parseInt(x.rusak);
        let tambahan = 0;
        
            if(parseInt(t) > parseInt(x.free_copy)){
                tambahan = parseInt(t) - parseInt(x.free_copy);
                 
            }
             
        $('.inv-tambahan').html(tambahan);
        $('.inv-tarif-eks').html(x.tarif_ekstra);
        $('.inv-tarif-tambahan').html( money.format(tambahan * parseInt(x.tarif_ekstra)) );
            let subtotal = 0;
            if(parseInt(t) > parseInt(x.free_copy)){
                
                subtotal = parseInt(x.tarif_sewa) + parseInt(x.tarif_copy) + (tambahan * parseInt(x.tarif_ekstra));      
                 
            }
            else{
                subtotal = parseInt(x.tarif_sewa) + parseInt(x.tarif_copy);
                
            }
            
        $('.inv-subtotal').html(money.format(subtotal));
            let ppn = subtotal * parseFloat(x.ppn) / 100;
            
        $('.inv-ppn').html(money.format(ppn));
        $('.inv-total').html(money.format(subtotal + ppn));
        $('.inv-meter-lama').html(x.meter_awal);
        $('.inv-meter-baru').html(x.meter_akhir);

        $('#inv-keterangan').html(x.keterangan);

        $('.modal-title').text('Nota Tagihan ' +'INV/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4)); // Set Title to Bootstrap modal title 
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
     
    

}


function print_invoice(){
    let i =$('#inv-no').html(); 
    $("#invoice").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: i,
        	doctype: '<!doctype html>'
	});
}

function invoice_transport(invoiceid)
{
    $('#modal_invoice_transport_preview').modal('show'); // show bootstrap modal
      
    $('#invoiceid_t').val(invoiceid); 
    
    
    $.ajax({	
    method:'POST',
	url: url+"/app/data/invoice",
	data: {id : invoiceid}
    })
	.done(function(data){        
		// console.log(data);        
        let x=JSON.parse(data);
        //console.log(x);

        let money = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 2
        })

        let bb = [ 'JANUARI',
                   'FEBRUARI',
                   'MARET',
                   'APRIL',
                   'MEI',
                   'JUNI',
                   'JULI',
                   'AGUSTUS',
                   'SEPTEMBER',
                   'OKTOBER',
                   'NOPEMBER',
                   'DESEMBER'                
                ];            
        $('#inv-bulan_t').html(bb[x.bulan - 1]);
        let d=new Date(Date.parse(x.created_at));         
        $('#inv-no_t').html('TRS/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4));
        $('#inv-nama_t').html(x.nama);
        $('#inv-perusahaan_t').html(x.perusahaan);
        if(!x.perusahaan)
        $('#inv-perusahaan_t').html(x.nama);
        $('#inv-alamat_t').html(x.alamat);
        $('#inv-prov_t').html(x.kota+', '+x.provinsi+', ');
        $('#inv-nopel_t').html(pad(x.customer_id,5));
        $('#inv-telp_t').html(x.telp);
        $('#inv-seri_t').html(x.seri);        
        let month = d.toLocaleString('id-ID', { month: 'long' });
         
        $('#inv-tanggal_t').html(d.getDate()+' '+month+' '+d.getFullYear());
        $('.inv-tarif-transport').html(money.format(x.transportasi));
        $('.inv-subtotal').html(money.format(x.transportasi));            
        $('.inv-ppn').html(money.format(0));
        $('.inv-total').html(money.format(x.transportasi));     
 

        $('.modal-title').text('Faktur Transport' +'TRS/'+d.getFullYear()+'/'+pad(d.getMonth()+1,2)+'/'+pad(x.id_invoice,4)); // Set Title to Bootstrap modal title 
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
     
    

}

function print_invoice_transport(){
    let i =$('#inv-no_t').html(); 
    $("#invoice-transport").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: i,
        	doctype: '<!doctype html>'
	});
}
function invoice_del(idlayanan){
    $('#idtagihan').val(idlayanan);      
    $('#idhapus').val(idlayanan);      
    $('#modal-delete').modal('show'); // show bootstrap modal
}
 
 
$('#formhapus').submit(function(e){
    e.preventDefault();
    console.log('hapus dimulai');
    let id=$('#idhapus').val();
    
        // add new pelanggan
        let data = { 
                id :id               
               };
         

        $.ajax({	
        url: url+"/panel/invoices/delete",
        data: data,
        type: 'POST'
        })
        .done(function(data){        
            console.log(data);  
            let d = JSON.parse(data);
            if(d.status == 1){
                console.log(data);
                alert( d.pesan );   
                $('#idhapus').val('');                
                $('#modal-delete').modal('hide');
                reload_table();
            }else{
                alert(data);
            }
        })
        .fail(function(xhr,status,error) {
            alert( "error "+error );
            console.log(error);
        })
        .always(function() {

        });

    
    
});




    function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
    </script>
<?= $this->endSection() ?>

