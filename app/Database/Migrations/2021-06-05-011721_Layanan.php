<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Layanan extends Migration
{
	public function up()
	{
		///// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_service'          => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'customer_id'       => [
				'type'           => 'INT',
				'constraint'     => '16',
				'unsigned'       => true
			],
			'com_id'      => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true
				
			],
			'meter_akhir'      => [
				'type'           => 'INT',
				'constraint'     => 255
				
			],
			'tarif'      => [
				'type'           => 'INT',
				'constraint'     => 255,
				'decimals'		=> 2,
				'default'		=> 0
			],
			'tarif_ekstra'      => [
				'type'           => 'INT',
				'constraint'     => 255,
				'decimals'		=> 2,
				'default'		=> 0
				
			],			
			'tarif_copy'      => [
				'type'           => 'int',
				'constraint'     => 255,
				'decimals'		=> 2,
				'default'		=> 0
				
			],
			'free_copy'      => [
				'type'           => 'int',
				'constraint'     => 255,
				'default'		=> 0
				
			],
			'ppn'      => [
				'type'           => 'int',
				'constraint'     => 255,
				'decimals'		=> 2,
				'default'		=> 0
				
			],
			'keterangan'      => [
				'type'           => 'TEXT'
				
			],		
			'last_invoice_id'      => [
				'type'           => 'INT'
			],
			'jumlah_tagihan'		=>[
				'type'		=> 'INT'
			],
			
			'created_at timestamp NULL Default current_timestamp NULL DEFAULT current_timestamp',
			'updated_at timestamp NULL',
			'deleted_at timestamp NULL',
		]);

		// Membuat primary key
		$this->forge->addKey('id_service', TRUE);

		$this->forge->addForeignKey('customer_id','mcustomers','id_customer');
		$this->forge->addForeignKey('com_id','mesin','id_m');

		// Membuat tabel news
		$this->forge->createTable('mservices', TRUE);
	}

	public function down()
	{
		$this->forge->dropTable('mservices',true);
	}
}
