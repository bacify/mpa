<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Mesin extends Migration
{
	public function up()
	{
		/// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_m'          => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'seri'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'merk'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			
			'keterangan'      => [
				'type'           => 'TEXT'				
			],
			'created_at timestamp NULL Default current_timestamp NULL DEFAULT current_timestamp',
			'updated_at timestamp NULL',
			'deleted_at timestamp NULL',
		]);

		// Membuat primary key
		$this->forge->addKey('id_m', TRUE);

		// Membuat tabel news
		$this->forge->createTable('mesin', TRUE);
	}

	public function down()
	{
		//
		$this->forge->dropTable('mesin',true);
	}
}
