<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pelanggan extends Migration
{
	public function up()
	{
		// Membuat kolom/field untuk tabel news
		$this->forge->addField([
			'id_customer'          => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true,
				'auto_increment' => true
			],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'nomorkartu'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'jeniskartu'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'alamat'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],			
			'desa'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'kecamatan'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'kota'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'provinsi'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'telp'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'created_at timestamp NULL Default current_timestamp NULL DEFAULT current_timestamp',
			'updated_at timestamp NULL',
			'deleted_at timestamp NULL',
		]);

		// Membuat primary key
		$this->forge->addKey('id_customer', TRUE);

		// Membuat tabel news
		$this->forge->createTable('mcustomers', TRUE);
	}

	public function down()
	{
		//drop table pelanggan
		$this->forge->dropTable('mcustomers',true);
	}
}
