<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tagihan extends Migration
{
	public function up()
	{
		//
		$this->forge->addField([
			'id_invoice'          => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true,
				'auto_increment' => true
			],			
			'layanan_id'      => [
				'type'           => 'INT',
				'constraint'     => 16,
				'unsigned'       => true,		
				
			],'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'nomorkartu'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'jeniskartu'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'alamat'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],			
			'desa'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'kecamatan'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'kota'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'provinsi'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'telp'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'seri'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
			'merk'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 255
				
			],
			'tarif_sewa'      => [
				'type'           => 'INT',
				'constraint'     => 255
				
			],
			'tarif_ekstra'      => [
				'type'           => 'INT',
				'constraint'     => 255
				
			],			
			'tarif_copy'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'free_copy'      => [
				'type'           => 'int',
				'constraint'     => 255
				
			],
			'meter_awal'      => [
				'type'           => 'INT',
				'constraint'     => 255
				
			],
			'meter_akhir'      => [
				'type'           => 'INT',
				'constraint'     => 255
				
			],		
			'rusak'		=>[
				'type'		=> 'INT'
			],			
			'ppn'		=>[
				'type'		=> 'INT'
			],
			'created_at timestamp NULL Default current_timestamp NULL DEFAULT current_timestamp',
			'updated_at timestamp NULL',
			'deleted_at timestamp NULL',
		]);

		// Membuat primary key
		$this->forge->addKey('id_invoice', TRUE);

		$this->forge->addForeignKey('layanan_id','mservices','id_service');		

		// Membuat tabel news
		$this->forge->createTable('minvoices', TRUE);
	}

	public function down()
	{
		//
		$this->forge->dropTable('minvoices',true);
	}
}
