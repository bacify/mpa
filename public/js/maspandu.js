async function getState(val) {
    
	return await $.ajax({	
	url: url+"/app/kabupaten",
	data: {prov : val}
    })
	.done(function(data){
        
		// console.log(data);
        let kota=$('#kota');
        let kec=$('#kec');
        let desa=$('#desa');
        kota.empty();
        kec.empty();
        desa.empty();        
        $.each(data,function(key,entry){
            // console.log( entry);
            kota.append(new Option(entry.name, entry.id));
        });
		// console.log('gs done '+val);
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
}

async function getDistrict(val) {
	
    return	await $.ajax({	
	url: url+"/app/kecamatan",
	data: {kab : val}
    })
	.done(function(data){        
		// console.log(data);        
        let kec=$('#kec');
        let desa=$('#desa');
        
        kec.empty();
        desa.empty();        
        $.each(data,function(key,entry){
            // console.log( entry);
            kec.append(new Option(entry.name, entry.id));
        });
		// console.log('gd done '+val);
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
}
async function getVillage(val) {
    return await $.ajax({	
        url: url+"/app/desa",
        data: {kec : val}
        })
        .done(function(data){        
            // console.log(data);        
             
            let desa=$('#desa');            
             
            desa.empty();        
            $.each(data,function(key,entry){
                // console.log( entry);
                desa.append(new Option(entry.name, entry.id));
            });
         //  console.log('gv done '+val);

        })
        .fail(function(xhr,status,error) {
            alert( "error " );
            console.log(error);
        })
        .always(function() {
            
        });
}

async function ngetState(val) {
    
	await $.ajax({	
	url: url+"/app/kabupaten",
	data: {prov : val}
    })
	.done(function(data){
        
		// console.log(data);
        let kota=$('#n-kota');
        let kec=$('#n-kec');
        let desa=$('#n-desa');
        kota.empty();
        kec.empty();
        desa.empty();        
        $.each(data,function(key,entry){
            // console.log( entry);
            kota.append(new Option(entry.name, entry.id));
        });
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
}

async function ngetDistrict(val) {
	
	await $.ajax({	
	url: url+"/app/kecamatan",
	data: {kab : val}
    })
	.done(function(data){        
		// console.log(data);        
        let kec=$('#n-kec');
        let desa=$('#n-desa');
        
        kec.empty();
        desa.empty();        
        $.each(data,function(key,entry){
            // console.log( entry);
            kec.append(new Option(entry.name, entry.id));
        });
		
	})
    .fail(function(xhr,status,error) {
        alert( "error " );
        console.log(error);
    })
    .always(function() {
        
    });
}
async function ngetVillage(val) {
	await $.ajax({	
        url: url+"/app/desa",
        data: {kec : val}
        })
        .done(function(data){        
            // console.log(data);        
             
            let desa=$('#n-desa');            
             
            desa.empty();        
            $.each(data,function(key,entry){
                // console.log( entry);
                desa.append(new Option(entry.name, entry.id));
            });
            
        })
        .fail(function(xhr,status,error) {
            alert( "error " );
            console.log(error);
        })
        .always(function() {
            
        });
}